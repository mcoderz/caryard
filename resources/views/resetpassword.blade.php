
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://parsleyjs.org/dist/parsley.js"></script>

<style>

input.parsley-success,
select.parsley-success,
textarea.parsley-success {
  color: #468847;
  background-color: #DFF0D8;
  border: 1px solid #D6E9C6;
}

input.parsley-error,
select.parsley-error,
textarea.parsley-error {
  color: #B94A48;
  background-color: #F2DEDE;
  border: 1px solid #EED3D7;
}

.parsley-errors-list {
  margin: 2px 0 3px;
  padding: 0;
  list-style-type: none;
  font-size: 0.9em;
  line-height: 0.9em;
  opacity: 0;
  color: #B94A48;

  transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -webkit-transition: all .3s ease-in;
}

.parsley-errors-list.filled {
  opacity: 1;
}
/* Style all input fields */
input {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
}

/* Style the submit button */
input[type=submit] {
  background-color: #4CAF50;
  color: white;
}

/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 10px 35px;
  font-size: 18px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: "✖";
}
</style>
</head>
<body>

<h2>Reset Password </h2>
@if(session()->has('change'))
                      <div class="error-display">
                      <h6 class="text-center" style="color:green;">
                      Password Change Successfully
                      </h6> 
                      </div>
                      @endif

<div class="container">
  <form action="{{url('resetPassword')}}" method="POST" enctype="multipart/form-data" class="form-horizontal style-form"  data-parsley-validate>
  @csrf
  <input type="hidden" id="changeId" name="id" value="{{$user_id}}">
    <label for="usrname">New Password (Minmum six characters)</label>
    <input type="password" id="newPassword" minlength="6" name="password" required="" >
	@if($errors->has('new_password'))
						<div class="text-white"><span>{{ $errors->first('new_password') }} </span></div>
						@endif 
    <label for="psw">Conform  Password (Minmum six characters)</label>
    <input type="password" id="ConfirmPassword" minlength="6" name="conformPassword"  required="">
	@if($errors->has('confirm_password'))
						<div class="text-white"><span>{{ $errors->first('confirm_password') }}</span></div>
						@endif 
    <span id="ConfirmPasswordcheck" style="color: red"></span>
    <span id="matchPassword" style="color: green"></span>

    <input type="submit" value="Submit">
  </form>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
			
<script>

$(function() {
    $(".timeMassage").hide(5000);

});

$( document ).ready(function() {
    var pathname = window.location.pathname;
    var  parts = pathname.split("/");
    var last_part = parts[parts.length-1];
    $('#changeId').val(last_part);
});

$(document).on('keyup','#ConfirmPassword',function(){
    // alert(this.value);
    var newPass = $('#newPassword').val();
    if(newPass == this.value){
      // alert("match")
      $('#matchPassword').html('').html('Password  Matched');
      $('#ConfirmPasswordcheck').html('').html('');
    }
    else{
      // alert("not match")
      $('#ConfirmPasswordcheck').html('').html('Password Not Matched');
      $('#matchPassword').html('').html(' ');
    }

  });



</script>

</body>
</html>
