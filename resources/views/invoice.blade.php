@extends('layouts.successlogin')
@section('content')
<section>
    <div id="page-content-wrapper" class=" mt-65">
        <div id="content">
            <div class="container-fluid p-0 px-lg-0 px-md-0">

                <!-- Begin Page Content -->
                <div class="container-fluid px-lg-4">
                    <div class="row">
                        <div class="col-md-12 mt-lg-4 mt-4">
                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">Invoice List</h1>
                                <!-- <a href="#" data-toggle="modal" data-target="#addInvoice"
                                    class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                    <i class="fas fa-plus" aria-hidden="true"></i>
                                    Add Invoice </a> -->
                            </div>
                        </div>

                        <!-- column -->

                        <!-- Datatable -->
                        <div class="col-md-12 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <div class="table-responsive">
                                            <table class="table v-middle" id="datatable">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="border-top-0">Sl.No.</th>
                                                        <th class="border-top-0">Name</th>
                                                        <th class="border-top-0">Email</th>
                                                        <th class="border-top-0">Amount </th>
                                                        <th class="border-top-0"> Plane Name </th>
                                                       
                                                    </tr>
                                                </thead>
                                                <?php $i = 1; ?>
                                                 @foreach($datas as $data)
                                                <tr>
                                                   
                                                    
                                                        <td>1</td>
                                                        <td>{{$data->name}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->amount}}</td>
                                                        <td>{{$data->plan_name}}</td>
                                                       

                                                    
                                                    </tr>
                                                
                                                    @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- **// main content -->

</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

<script>
$(document).ready(function() {
    $('#myTable').DataTable();
});
</script>
@stop