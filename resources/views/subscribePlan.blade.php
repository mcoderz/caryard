@extends('layouts.successlogin')
@section('content')
<section>
    <div id="page-content-wrapper" class=" mt-65">
        <div id="content">
            <div class="container-fluid p-0 px-lg-0 px-md-0">

                <!-- Begin Page Content -->
                <div class="container-fluid px-lg-4">
                    <div class="row">
                        <div class="col-md-12 mt-lg-4 mt-4">
                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">Subscribe Plan</h1>
                                <a href="#" data-toggle="modal" data-target="#addInvoice"
                                    class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                    <i class="fas fa-plus" aria-hidden="true"></i>
                                    Add Plan </a>
                            </div>
                        </div>

                        <!-- column -->

                        <!-- Datatable -->
                        <div class="col-md-12 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <div>
                                        <div class="table-responsive">
                                            <table class="table v-middle" id="datatable">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="border-top-0">Sl.No.</th>
                                                        <th class="border-top-0">Name</th>
                                                        <th class="border-top-0">Amount</th>
                                                        <th class="border-top-0"> Month </th>
                                                        <th class="border-top-0"> Post Count </th>
                                                        <th class="border-top-0"> Action </th>
                                                    </tr>
                                                </thead>
                                                <?php $i = 1; ?>
                                                <tbody>
                                                    @foreach($allSubscribe as $allinvoice)
                                                    <tr>
                                                        <th>{{$i}}</th>
                                                        <th>{{$allinvoice->plan_name}}</th>
                                                        <th>{{$allinvoice->amount}}</th>
                                                        <th>{{$allinvoice->month}}</th>
                                                        <th>{{$allinvoice->post_count}}</th>
                                                        <th>
                                                        <a href="#"data-toggle="modal"data-target="#editInvoice" data-id="{{$allinvoice->id}}"data-invoicename="{{$allinvoice->plan_name}}"     data-amount="{{$allinvoice->amount}}"data-duration="{{$allinvoice->month}}"data-description="{{$allinvoice->post_count}}" class="btn btn-success btn-circle-custome editinvoice">
                                                        <i class="fas fa-edit "></i>                                                        </a>
                                                            <a href="#" data-toggle="modal" data-target="#delete_modal" class="btn btn-danger btn-circle-custome delete" data-id="{{$allinvoice->id}}">
                                                            <i class="fas fa-trash-alt "></i>
                                                            </a>
                                                        </th>

                                                    </tr>
                                                    @endforeach

                                                </tbody>
                                                <tfoot>
                                                    <tr>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- **// main content -->

</section>

<!-- add model -->
<div class="modal fade" id="addInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Plan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url('/admin/addSubscribePlan') }}">
                  {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planName">Invoice Name</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="invoicename" name="name" type="text" placeholder="Plan Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planAmount">Amount</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="amount" name="amount"  type="text" placeholder="Amount" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planDuration">Month</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="duration" name="month" type="text" placeholder="Duration">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                            for="planEndDate">Post Count</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <!-- <textarea class="form-control" name="postcount" id="description" cols="30" rows="3"></textarea> -->
                            <input class="form-control" id="postcount" name="postcount" type="text" placeholder="post count">

                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save </button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end model  -->

<!-- add model -->
<div class="modal fade" id="editInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Plan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url('/admin/editSubscribePlan') }}">
                  {{ csrf_field() }}
                  <input type="hidden" id ="id" name ="id" >
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planName">Invoice Name</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="invoicename1" name="name" type="text" placeholder="Plan Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planAmount">Amount</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="amount1" name="amount"  type="text" placeholder="Amount">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planDuration">Month</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <input class="form-control" id="month" name="month" type="text" placeholder="Month">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                            for="planEndDate">Post Count</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                            <!-- <textarea class="form-control" name="description" id="description1" cols="30" rows="3"></textarea> -->
                            <input class="form-control" id="postcount1" name="postcount" type="text" placeholder="Post Count">

                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save </button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- end model  -->
<!-- delete -->
<div class="modal fade" id="delete_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal">&#10006;</button>
            </div>
            <input type="hidden" id="id1" value="">
            <!-- Modal footer -->
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="deleteData">Delete</button>
            </div>

        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

<script>

$(document).on('click', '.delete', function() {
    var id = $(this).attr('data-id');
    $('#id1').val(id);
    // $('#delete_modal').modal('show');
});
$(document).on('click', '#deleteData', function() {
    var id = $('#id1').val();

    $.ajax({
        type: "POST",
        url: "{{url('admin/SubscribePlanDelete')}}",
        data: {
            "_token": "{{ csrf_token() }}",
            "id": id
        },
        success: function(data) {
            location.reload();
        }
    });
});

$(document).on('click','.editinvoice',function(){
    var id = $(this).attr('data-id');
    var invoicename = $(this).attr('data-invoicename');
    var amount = $(this).attr('data-amount');
    var duration = $(this).attr('data-duration');
    var description = $(this).attr('data-description');

    $('#id').val(id);
    $('#invoicename1').val(invoicename);
    $('#amount1').val(amount);
    $('#month').val(duration);
    $('#postcount1').val(description);
});

$(document).ready(function() {
    $('#myTable').DataTable();
});
</script>
@stop