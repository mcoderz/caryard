@extends('layouts.successlogin')
@section('content')
<section>
            <div id="page-content-wrapper" class=" mt-65">
                <div id="content">
                    <div class="container-fluid p-0 px-lg-0 px-md-0">

                        <!-- Begin Page Content -->
                        <div class="container-fluid px-lg-4">
                            <div class="row">
                                <div class="col-md-12 mt-lg-4 mt-4">
                                    <!-- Page Heading -->
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Blank Page</h1>
                                        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                                class="fas fa-download fa-sm text-white-50"></i>
                                            Generate Report</a>
                                    </div>
                                </div>

                                <!-- column -->

                                <!-- Table and Form Insert here -->
                                <div class="col-md-12">
                                    <div class="sub-content">

                                    </div>
                                </div>
                                <!-- Table and Form Insert here -->

                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </div>

                </div>
            </div>
        </section>