@extends('layouts.successlogin')
@section('content')
<section>
            <div id="page-content-wrapper" class=" mt-65">
                <div id="content">
                    <div class="container-fluid p-0 px-lg-0 px-md-0">

                        <!-- Begin Page Content -->
                        <div class="container-fluid px-lg-4">
                            <div class="row">
                                <div class="col-md-12 mt-lg-4 mt-4">
                                    <!-- Page Heading -->
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Plans List</h1>
                                        <a href="#"
                                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
                                            data-toggle="modal" data-target="#addPlan">
                                            <i class="fas fa-plus" aria-hidden="true"></i>
                                            Add Plan</a>
                                    </div>
                                </div>

                                <!-- column -->

                                <!-- Datatable -->
                                <div class="col-md-12 mb-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <!-- title -->
                                            <!-- <div class="d-md-flex align-items-center">
                                                <div>
                                                    <h4 class="card-title">DataTable</h4>
                                                    <h5 class="card-subtitle">Overview of Top Selling Items</h5>
                                                </div>
                                                <div class="ml-auto">
                                                    <div class="dl">
                                                        <select class="custom-select">
                                                            <option value="0" selected="">Monthly</option>
                                                            <option value="1">Daily</option>
                                                            <option value="2">Weekly</option>
                                                            <option value="3">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- title -->
                                        
                                        <div>
                                            <div class="table-responsive">
                                                <table class="table v-middle" id="datatable">
                                                    <thead>
                                                        <tr class="bg-light">
                                                            <th class="border-top-0">Language</th>
                                                            <th class="border-top-0">Name</th>
                                                            <th class="border-top-0">Amount</th>
                                                            <th class="border-top-0">Duration/End Date</th>
                                                            <th class="border-top-0" width="250">Description</th>
                                                            <th class="border-top-0">Action</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>English</td>
                                                            <td>
                                                                Plan Name
                                                            </td>
                                                            <td> 500.00 </td>
                                                            <td>
                                                                2 : 23/01/2021
                                                            </td>
                                                            <td> Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi fugiat, iste illum magnam nisi laborum debitis earum praesentium soluta ipsa, iusto adipisci velit error dignissimos nesciunt impedit quaerat tempore eligendi! </td>
                                                            <td>
                                                                <ul
                                                                    class="list-unstyled d-flex justify-content-center align-content-center">
                                                                    <li class="px-1">
                                                                        <a href="#"data-toggle="modal" data-target="#addPlan" class="btn btn-success btn-circle-custome">
                                                                            <i class="fas fa-edit "></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="px-1">
                                                                        <a href="#"data-toggle="modal" data-target="#addPlan" class="btn btn-info btn-circle-custome">
                                                                            <i class="fas fa-eye "></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="px-1">
                                                                        <a href="#" class="btn btn-danger btn-circle-custome">
                                                                            <i class="fas fa-trash-alt "></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                           
                                                        </tr>
                                                        <tr>
                                                            <td>Arabic</td>
                                                            <td>
                                                                Plan Name
                                                            </td>
                                                            <td> 500.00 </td>
                                                            <td>
                                                                2 : 23/01/2021
                                                            </td>
                                                            <td> Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi fugiat, iste illum magnam nisi laborum debitis earum praesentium soluta ipsa, iusto adipisci velit error dignissimos nesciunt impedit quaerat tempore eligendi! </td>
                                                            <td>
                                                                <ul
                                                                    class="list-unstyled d-flex justify-content-center align-content-center">
                                                                    <li class="px-1">
                                                                        <a href="#"data-toggle="modal" data-target="#addPlan" class="btn btn-success btn-circle-custome">
                                                                            <i class="fas fa-edit "></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="px-1">
                                                                        <a href="#"data-toggle="modal" data-target="#addPlan" class="btn btn-info btn-circle-custome">
                                                                            <i class="fas fa-eye "></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="px-1">
                                                                        <a href="#" class="btn btn-danger btn-circle-custome">
                                                                            <i class="fas fa-trash-alt "></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                           
                                                        </tr>
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Datatable -->

                            </div>
                            <!-- Table and Form Insert here -->

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>

            </div>
            </div>
        </section>
        <!-- /#page-content-wrapper -->
       
         <!-- Add Contact Popup -->
        <!-- Modal -->
<div class="modal fade" id="addPlan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Plan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <form action="">
      <div class="modal-body">
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planName">Language</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <div class="form-control border-0">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="radio-inline" checked="">
                                <span class="custom-control-label">English</span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="radio-inline" >
                                <span class="custom-control-label">Arabic</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planName">Plan Name</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <input class="form-control" id="planName" type="text"
                            placeholder="Plan Name">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planAmount">Amount</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <input class="form-control" id="planAmount" type="text"
                            placeholder="Amount">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planDuration">Duration</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <input class="form-control" id="planDuration" type="text"
                            placeholder="Duration">
                    </div>
                </div> 
                
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planEndDate">End Date</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <input class="form-control" id="planEndDate" type="date"
                            placeholder="EndDate">
                    </div>
                </div> 
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planEndDate">Description</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                    <textarea class="form-control" name="" id="planEndDate" cols="30" rows="3"></textarea>
                    </div>
                </div> 
                
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Save </button>
      </div>
      
      </form>
    </div>
  </div>
</div>
