@extends('layouts.successlogin')
@section('content')
<!--  main content -->
<style>
#content {
    width: 100%;
    width: calc(100% - var(--sidebar-width));
    margin-left: var(--sidebar-width);
    transition: all 0.5s ease;
}
.img-wrapper{
 overflow:hidden;
}
.img-wrapper img{

    width:100%;
    height:100%;
    object-fit:cover;
}
</style>
<section>
    <div id="content">
        <!--  title start -->
        <div class="title-bar">
            <div class="row align-items-center">
                <div class="col-12 align-middle">
                    <ul class="view-title">
                        <li><a href="javascript:void(0);">User data</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!--  collapse -->
        <div id="accordion" class="myaccordion">
            @foreach($allcars as $allcar)
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                            data-toggle="collapse" data-target="#collapseOne_{{$allcar->id}}" aria-expanded="false"
                            aria-controls="collapseOne">
                            {{$allcar->brand_name}}
                            <span class="fa-stack fa-sm">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </button>
                    </h2>
                </div>
                <div id="collapseOne_{{$allcar->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Name</label>
                                        <div class="col-sm-7">
                                        {{$allcar->brand_name}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Brand Name <br><small>(in
                                                English)</small></label>
                                        <div class="col-sm-7">
                                        {{$allcar->brand_name}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-2"></div> -->
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Model Name <br> <small>(in
                                                Arabic)</small></label>
                                        <div class="col-sm-7">
                                        {{$allcar->model_name}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Description <br></label>
                                        <div class="col-sm-7">
                                        {{$allcar->description}}

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Year <br> </label>
                                        <div class="col-sm-7">
                                        {{$allcar->year}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Import</label>
                                        <div class="col-sm-7">
                                        {{$allcar->import}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Price</label>
                                        <div class="col-sm-7">
                                        {{$allcar->price}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Mileage</label>
                                        <div class="col-sm-7">
                                        {{$allcar->mileage}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Cylinder</label>
                                        <div class="col-sm-7">
                                        {{$allcar->cylinder}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Transmission Name</label>
                                        <div class="col-sm-7">
                                        {{$allcar->transmission_name}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Interior Color</label>
                                        <div class="col-sm-7">
                                        {{$allcar->int_color}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Exterior Color</label>
                                        <div class="col-sm-7">
                                        {{$allcar->ext_color}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h4>Images </h4>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($allcar->images as $img)
                                <div class="col-md-4">
                                    <div class="img-wrapper">
                                        <!-- <img src="url({{$img->image}})"> -->
                                        <img src="{{url($img->image)}}">

                                    </div>
                                </div>
                            @endforeach  
                            <!-- <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                            aria-controls="collapseTwo">
                            B
                            <span class="fa-stack fa-2x">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Name</label>
                                        <div class="col-sm-7">
                                            A
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Brand Name <br><small>(in
                                                English)</small></label>
                                        <div class="col-sm-7">
                                            A
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Model Name <br> <small>(in
                                                Arabic)</small></label>
                                        <div class="col-sm-7">
                                            model B
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Description <br></label>
                                        <div class="col-sm-7">
                                            Dfgdfgdfgg

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Year <br> </label>
                                        <div class="col-sm-7">
                                            2021
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Import</label>
                                        <div class="col-sm-7">
                                            Sdfd
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Price</label>
                                        <div class="col-sm-7">
                                            50
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Mileage</label>
                                        <div class="col-sm-7">
                                            6
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Cylinder</label>
                                        <div class="col-sm-7">
                                            8
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Transmission Name</label>
                                        <div class="col-sm-7">
                                            IN
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Interior Color</label>
                                        <div class="col-sm-7">
                                            BLack
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Exterior Color</label>
                                        <div class="col-sm-7">
                                            Black
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h4>Images </h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed"
                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                            aria-controls="collapseThree">
                            C
                            <span class="fa-stack fa-2x">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Name</label>
                                        <div class="col-sm-7">
                                            A
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Brand Name <br><small>(in
                                                English)</small></label>
                                        <div class="col-sm-7">
                                            A
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Model Name <br> <small>(in
                                                Arabic)</small></label>
                                        <div class="col-sm-7">
                                            model B
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-5 col-form-label">Description <br></label>
                                        <div class="col-sm-7">
                                            Dfgdfgdfgg

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Year <br> </label>
                                        <div class="col-sm-7">
                                            2021
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Import</label>
                                        <div class="col-sm-7">
                                            Sdfd
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Price</label>
                                        <div class="col-sm-7">
                                            50
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Mileage</label>
                                        <div class="col-sm-7">
                                            6
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Cylinder</label>
                                        <div class="col-sm-7">
                                            8
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Transmission Name</label>
                                        <div class="col-sm-7">
                                            IN
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Interior Color</label>
                                        <div class="col-sm-7">
                                            BLack
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col_selector">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Exterior Color</label>
                                        <div class="col-sm-7">
                                            Black
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h4>Images </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img-wrapper">
                                    <img src="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- collapse-->




    </div>
    </div>
    </div>
</section>

<script>
$("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
    $(e.target)
        .prev()
        .find("i:last-child")
        .toggleClass("fa-minus fa-plus");
});
</script>

<!-- Add Types -->
@stop