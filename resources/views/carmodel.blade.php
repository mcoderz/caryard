@extends('layouts.successlogin')
@section('content')
<style>
/* Tabs */
.carmodel-list .nav-item .nav-link {
    color: #000 !important;
}

/* Just for CodePen styling - don't include if you copy paste */
/* accordion */

.accordion-container {
    position: relative;
    height: auto;
    margin: 10px auto;
}

.accordion-container>h2 {
    text-align: center;
    color: #fff;
    padding-bottom: 5px;
    margin-bottom: 20px;
    padding-bottom: 15px;
    border-bottom: 1px solid #ddd;
}

.set {
    position: relative;
    width: 100%;
    height: auto;
    background-color: #f5f5f5;
    margin-bottom: 20px;
}

.set>a {
    display: block;
    padding: 10px 15px;
    text-decoration: none;
    color: #555;
    font-weight: 600;
    border-bottom: 1px solid #ddd;
    -webkit-transition: all 0.2s linear;
    -moz-transition: all 0.2s linear;
    transition: all 0.2s linear;
}

.set>a i {
    float: right;
    margin-top: 2px;
}

.set>a.active {
    background-color: #3399cc;
    color: #fff;
}

.content {
    background-color: #fff;
    border-bottom: 1px solid #ddd;
    display: none;
}

.content p {
    padding: 10px 15px;
    margin: 0;
    color: #333;
}

.brand-wrapper {
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.brand-form {
    padding: 20px;
}

.brand-col {
    margin-bottom: 10px;
}

.brand-col label {
    width: 100px;
    display: inline-block;
}

.modal-content {
    border-radius: 10px;
}

.listing-wrap {
    margin-top: 30px;
    padding: 0;
    list-style: none;
}

.listing-wrap li {
    padding: 15px;
    margin-bottom: 10px;
    background-color: #f5f5f5;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.edit-delete {
    display: inline-block;
}

.edit-delete a {
    margin: 0 5px;
}

.edit-delete a .fa-trash-alt {
    color: red;
}

.accordion-container .edit-delete {
    display: flex;
    justify-content: flex-end;
    margin-top: 5px;
}
</style>
<section>
    <div id="page-content-wrapper" class=" mt-65">
        <div id="content">
            <div class="container-fluid p-0 px-lg-0 px-md-0">

                <!-- Begin Page Content -->
                <div class="container-fluid px-lg-4">
                    <div class="row">
                        <div class="col-md-12 mt-lg-4 mt-4">
                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">Car Model Details</h1>

                            </div>
                        </div>

                        <!-- column -->

                        <!-- Datatable -->
                        <div class="col-md-12 mb-4">
                            <div class="card">
                                <div class="card-body carmodel-list">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#brand"
                                                role="tab">Brand</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#year" role="tab">Year</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#import" role="tab">Import</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#cylinder"
                                                role="tab">Cylinders</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#transmission"
                                                role="tab">Transmission</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#body" role="tab">Body
                                                Type</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#exterior" role="tab">Exterior
                                                Color</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#interior" role="tab">Interior
                                                Color</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="brand" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Brand / Modal</h4>

                                                <div class="add-more">
                                                    <a href="javascript:void(0);" class="btn btn-primary"
                                                        data-toggle="modal" data-target="#basicModal">Add More+</a>
                                                </div>
                                            </div>

                                            <div class="accordion-container pt-4">
                                                <div class="set">
                                                    <a href="#">
                                                        Hyundai
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="content">
                                                        <span class="edit-delete">
                                                            <a href=""><i class="fas fa-edit"></i></a>
                                                            <a href=""><i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </span>
                                                        <ul class="list-model">
                                                            <li>Grand P10</li>
                                                            <li>Creta</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="set">
                                                    <a href="#">
                                                        Chevrolet
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="content">
                                                        <span class="edit-delete">
                                                            <a href=""><i class="fas fa-edit"></i></a>
                                                            <a href=""><i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </span>
                                                        <ul class="list-model">
                                                            <li>Grand P10</li>
                                                            <li>Creta</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="set">
                                                    <a href="#">
                                                        Hyundai
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="content">
                                                        <span class="edit-delete">
                                                            <a href=""><i class="fas fa-edit"></i></a>
                                                            <a href=""><i class="fas fa-trash-alt"></i>
                                                            </a>
                                                        </span>
                                                        <ul class="list-model">
                                                            <li>Grand P10</li>
                                                            <li>Creta</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="year" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Year</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#yearpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>2021
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="import" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Import</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#importpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>Kuwait
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="cylinder" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Cylinders</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#cylinderpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>6
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="transmission" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Transmission</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#transmissionpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>Automatic
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="body" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Body Type</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#bodypop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>Sport
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="exterior" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Exterior Color</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#exteriorpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>Black
                                                    <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="interior" role="tabpanel">
                                            <div class="brand-wrapper pt-4">
                                                <h4>Interior Color</h4>

                                                <div class="add-more">
                                                    <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#interiorpop">Add More+</button>
                                                </div>
                                            </div>
                                            <ul class="listing-wrap">
                                                <li>Black <span class="edit-delete">
                                                        <a href=""><i class="fas fa-edit"></i></a>
                                                        <a href=""><i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </span></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- END tabs -->

                                </div>
                            </div>
                        </div>
                        <!-- End Datatable -->
                    </div>
                    <!-- Table and Form Insert here -->
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>

    </div>
    </div>
</section>
<!-- /#page-content-wrapper -->
<!-- basic modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Brand & Model</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Brand</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="brand-col">
                    <label>Model</label>
                    <input type="text" class="form-control" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>



<div class="modal fade" id="yearpop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add year</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Year</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="importpop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Import</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Import</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="cylinderpop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Cylinders</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Cylinders</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="transmissionpop" tabindex="-1" role="dialog" aria-labelledby="basicModal"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Transmission</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Transmission</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="bodypop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Body Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Body Type</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="exteriospop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Exterior Color</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Exterior Color</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal fade" id="interiorpop" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Interior Color</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="brand-form">
                <div class="brand-col">
                    <label>Interior Color</label>
                    <input type="text" class="form-control" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Add</button>
                </div>

            </form>

        </div>
    </div>
</div>
<!-- Add Contact Popup -->

@stop
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $(".set > a").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).siblings(".content").slideUp(200);
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
        } else {
            $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
            $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
            $(".set > a").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp(200);
            $(this).siblings(".content").slideDown(200);
        }
    });

});
// Quick & dirty toggle to demonstrate modal toggle behavior
</script>
<script>
$(document).on('click', '.reply', function() {
    var id = $(this).attr('data-id');
    var email = $(this).attr('data-email');
    // alert(email)
    $('#id').val(id)
    $('#email').val(email)
    $('#exampleModalLabel').html('Admin Reply to ' + email);
})
</script>