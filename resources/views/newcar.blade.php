@extends('layouts.successlogin')
@section('content')
<section>
            <div id="page-content-wrapper" class=" mt-65">
                <div id="content">
                    <div class="container-fluid p-0 px-lg-0 px-md-0">

                        <!-- Begin Page Content -->
                        <div class="container-fluid px-lg-4">
                            <div class="row">
                                <div class="col-md-12 mt-lg-4 mt-4">
                                    <!-- Page Heading -->
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Add Car</h1>
                                        <a href="caiteria-list.php"
                                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
                                            >
                                            <i class="fas fa-eye" aria-hidden="true"></i>
                                            View List</a>
                                    </div>
                                </div>

                                <!-- column -->

                                <!-- Datatable -->
                                <div class="col-md-12 mb-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <!-- title -->
                                            <!-- <div class="d-md-flex align-items-center">
                                                <div>
                                                    <h4 class="card-title">DataTable</h4>
                                                    <h5 class="card-subtitle">Overview of Top Selling Items</h5>
                                                </div>
                                                <div class="ml-auto">
                                                    <div class="dl">
                                                        <select class="custom-select">
                                                            <option value="0" selected="">Monthly</option>
                                                            <option value="1">Daily</option>
                                                            <option value="2">Weekly</option>
                                                            <option value="3">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- title -->

                                            <div>
                                                <form action="">
                                                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right"
                        for="planName">Language</label>
                    <div class="col-12 col-sm-8 col-lg-8">
                        <div class="form-control border-0">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="radio-inline" checked="">
                                <span class="custom-control-label">English</span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" type="radio" name="radio-inline" >
                                <span class="custom-control-label">Arabic</span>
                            </label>
                        </div>
                    </div>
                </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="brandName">Brand Name</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1" >-- Select Brand Name --
                                                                </option>
                                                                <option value="2" selected="">Hyundai</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="model">Model</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1">-- Select Model --
                                                                </option>
                                                                <option value="2">Santro</option>
                                                                <option value="3">i10</option>
                                                                <option value="4">i20</option>
                                                                <option value="5" selected="">venue</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="year">Year</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <input class="form-control" id="year" type="text"
                                                                placeholder="2021">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="import">Import</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1" >-- Select Import --
                                                                </option>
                                                                <option value="2" selected>Kuwait</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="price">Price</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <input class="form-control" id="price" type="text"
                                                                placeholder="price">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12 col-sm-3"></div>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="checkbox" checked
                                                                    name="timelyMaintenanceRecord" id="inlineRadio1"
                                                                    value="option1">
                                                                <label class="form-check-label text-primary"
                                                                    for="inlineRadio1">Timely Maintenance Record</label>
                                                            </div>
                                                            <div class="form-check form-check-inline text-success">
                                                                <input class="form-check-input" type="checkbox" checked
                                                                    name="approvedbySupplier" id="inlineRadio2"
                                                                    value="option2">
                                                                <label class="form-check-label text-success"
                                                                    for="inlineRadio2">Approved by Supplier</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="mileage">Mileage</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <input class="form-control" id="mileage" type="text"
                                                                placeholder="Mileage">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="Cylinders">Cylinders</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1" >-- Select Cylinders --
                                                                </option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5"selected>5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="Transmission">Transmission</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1" >-- Select Transmission --
                                                                </option>
                                                                <option value="2" selected>Automatic</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="bodyType">Body Type</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1" >-- Select Body Type --
                                                                </option>
                                                                <option value="2" selected>Sports</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="exteriorColor">Exterior Color</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1">-- Select Exterior Color
                                                                    --</option>
                                                                <option value="2">Red</option>
                                                                <option value="3" selected>Black</option>
                                                                <option value="4">Blue</option>
                                                                <option value="5">Gray</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="interiorColor">Interior Color</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <select class="form-control">
                                                                <option value="1">-- Interior Color --
                                                                </option>
                                                                <option value="2">Red</option>
                                                                <option value="3" selected>Black</option>
                                                                <option value="4">Blue</option>
                                                                <option value="5">Gray</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="vnNumber">VN Number</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <input class="form-control" id="vnNumber" type="text"
                                                                placeholder="vnNumber">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="description">Description</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <textarea name="" id="" cols="30" rows="4"
                                                                class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"
                                                            for="photo">Photo</label>
                                                        <div class="col-12 col-sm-8 col-lg-8">
                                                            <div class="field" align="left">

                                                                <div class="custom-file">
                                                                    <input type="file" id="files" name="files[]"
                                                                        multiple class="custom-file-input "
                                                                        id="validatedCustomFile" required>
                                                                    <label class="custom-file-label filesLabel"
                                                                        for="validatedCustomFile">Choose
                                                                        Images...</label>
                                                                </div>
                                                            </div>
                                                            <div id="uploadFactoryImage" class="mb-2"></div>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12 col-sm-3" ></div>
                                                        <div class="col-12 col-sm-9" >
                                                        
                                                            <button class="btn btn-primary">Submit</button>
                                                        
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Datatable -->

                            </div>
                            <!-- Table and Form Insert here -->

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>

            </div>
            </div>
        </section>
        <!-- /#page-content-wrapper -->
        


<script>
$(document).ready(function() {
    if (window.File && window.FileList && window.FileReader) {
        $("#files").on("change", function(e) {
            var files = e.target.files,
                filesLength = files.length;
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                    var file = e.target;
                    $("<span class=\"pip\">" +
                        "<img class=\"img-fluid\" src=\"" + e.target.result +
                        "\" title=\"" + file.name + "\"/>" +
                        "<br/><span class=\"remove\"><i class=\"far fa-times-circle\"></i></span>" +
                        "</span>").insertAfter("#uploadFactoryImage");
                    $(".remove").click(function() {
                        $(this).parent(".pip").remove();
                    });
                    
                    // Old code here
                    /*$("<img></img>", {
                      class: "imageThumb",
                      src: e.target.result,
                      title: file.name + " | Click to remove"
                    }).insertAfter("#files").click(function(){$(this).remove();});*/

                });
                fileReader.readAsDataURL(f);
            }
            console.log(files);
        });
    } else {
        alert("Your browser doesn't support to File API")
    }
});
        </script> 