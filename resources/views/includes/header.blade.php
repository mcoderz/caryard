<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="{{url('../assets/css/bootstrap.min.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700;800;900&display=swap" rel="stylesheet">
    <title>Admin Template</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="assets/css/perfect-scrollbar.css"> -->
    <link rel="stylesheet" href="{{url('../assets/css/perfect-scrollbar.css')}}" />
    <!--fontawesome-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <!-- Datatable Css -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
     <!-- Datetimepicker -->
     <!-- <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css"> -->
     <link rel="stylesheet" href="{{url('../assets/css/bootstrap-datetimepicker.min.css')}}" />
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css">
    
    <!-- <link rel="stylesheet" href="assets/css/main-style.css"> -->
    <link rel="stylesheet" href="{{url('../assets/css/main-style.css')}}" />
    <style>

    </style>
</head>

<body>
    <div id="wrapper">
        <div class="overlay"></div>