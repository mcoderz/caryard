<section>
            <footer id="footer" class="footer">
                <div class="container-fluid">
                    <div class="row text-muted">
                        <div class="col-md-6 text-center text-md-left">
                            <p class="mb-0">
                                <a href="#" class="text-muted"><strong>Dashboard Mobilecoderz Design
                                </strong></a> &copy
                            </p>
                        </div>
                        <div class="col-md-6 text-center text-md-right">
                            <ul class="list-inline">
                                <li class="footer-item">
                                    <a class="text-muted" href="#">Support</a>
                                </li>
                                <li class="footer-item">
                                    <a class="text-muted" href="#">Help Center</a>
                                </li>
                                <!-- <li class="footer-item">
                                    <a class="text-muted" href="#">Privacy</a>
                                </li>
                                <li class="footer-item">
                                    <a class="text-muted" href="#">Terms</a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </section>

        </div>
    <!-- /#wrapper -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="assets/js/jquery-3.0.0.min.js"></script> -->
	<script src="{{url('../assets/js/jquery-3.0.0.min.js')}}"></script>
    <!-- <script src="assets/js/popper.min.js"></script> -->
	<script src="{{url('../assets/js/popper.min.js')}}"></script>
    <!-- <script src="assets/js/bootstrap.min.js"></script> -->
	<script src="{{url('../assets/js/bootstrap.min.js')}}"></script>
    <!-- <script src="assets/js/perfect-scrollbar.jquery.js"></script> -->
	<script src="{{url('../assets/js/perfect-scrollbar.jquery.js')}}"></script>
     <!-- datetimepicker -->
     <!-- <script src="assets/js/bootstrap-datetimepicker.min.js"></script> -->
	 <script src="{{url('../assets/js/bootstrap-datetimepicker.min.js')}}"></script>
      <!-- Datatable -->
    <!-- <script src="assets/js/jquery.dataTables.min.js"></script> -->
	<script src="{{url('../assets/js/jquery.dataTables.min.js')}}"></script>
    <!-- <script src="assets/js/dataTables.bootstrap4.min.js"></script> -->
	<script src="{{url('../assets/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js"></script>
    <!-- <script src="assets/js/custome-script.js"></script> -->
	<script src="{{url('../assets/js/custome-script.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script>
        
$('#sidebar_menu  a').filter(function(){
    return this.href==location.href
    }).addClass('active').siblings().removeClass('active');

    $('#sidebar_menu ul a').click(function(){
    $(this).addClass('active').siblings().removeClass('active')    
    });
    </script>
</body>

</html>