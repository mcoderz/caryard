<section>
    <nav class="fixed-top align-top" id="sidebar-wrapper" role="navigation">
        <div class="simplebar-content" style="padding: 0px;">
            <a class="sidebar-brand" href="{{url('admin/dashboard')}}">
                <!-- <span class="align-middle">Admin Template</span> -->
                <img src="{{url('../assets/images/logo.png')}}" alt="logo" class="img-fluid align-middle"
                    style="height:40px;" />
            </a>

            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul id="sidebar_menu" class="navbar-nav align-self-stretch sidebar-elements">
                        <!-- <li class="sidebar-header">
                                Pages
                            </li> -->
                        <li class="">
                            <a href="{{url('admin/dashboard')}}" class="nav-link text-left" role="button"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-tachometer-alt"></i> Dashboard
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('admin/contactus')}}" class="nav-link text-left" role="button">
                                <i class="far fa-address-book"></i> Contact Us
                            </a>
                        </li>

                        <li class="">
                            <a href="{{url('admin/userlist')}}" class="nav-link text-left" role="button">
                                <i class="fas fa-users"></i> User
                            </a>
                        </li>

                        <li class="">
                            <a href="{{url('admin/subscribePlan')}}" class="nav-link text-left" role="button">
                                <i class="fas fa-users"></i> Subscribe Plan
                            </a>
                        </li>

                        <li class="">
                            <a href="{{url('admin/invoice')}}" class="nav-link text-left" role="button">
                                <i class="fas fa-tasks"></i> Invoice
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('admin/criteria')}}" class="nav-link text-left" role="button">
                                <i class="fas fa-clipboard-check"></i> Favorite Criteria
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('admin/carmodel')}}" class="nav-link text-left" role="button">
                                <i class="fas fa-car"></i> Car Model
                            </a>
                        </li>
                        <!-- <li class="">   
                                    <a href="{{url('admin/newcar')}}" class="nav-link text-left" role="button">
                                    <i class="fas fa-car"></i> New Car
                                    </a>
                                </li>

                               

                                <li class="">
                                    <a href="{{url('admin/blankpage')}}" class="nav-link text-left" role="button">
                                        <i class="fas fa-file"></i> Blank page
                                    </a>
                                </li> -->




                    </ul>
                </div>
            </div>

        </div>
    </nav>
</section>