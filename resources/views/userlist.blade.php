@extends('layouts.successlogin')
@section('content')
<section>
    <div id="page-content-wrapper" class=" mt-65">
        <div id="content">
            <div class="container-fluid p-0 px-lg-0 px-md-0">

                <!-- Begin Page Content -->
                <div class="container-fluid px-lg-4">
                    <div class="row">
                        <div class="col-md-12 mt-lg-4 mt-4">
                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">User List</h1>

                            </div>
                        </div>

                        <!-- column -->

                        <!-- Datatable -->
                        <div class="col-md-12 mb-4">
                            <div class="card">
                                <div class="card-body">

                                    <!-- title -->

                                    <div>
                                        <div class="table-responsive">
                                            <table class="table v-middle" id="datatable">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="border-top-0">Sl.No.</th>
                                                        <th class="border-top-0">User ID</th>
                                                        <th class="border-top-0">User Name</th>
                                                        <th class="border-top-0"> Phone No </th>
                                                        <th class="border-top-0">Email ID</th>
                                                        <!-- <th class="border-top-0">Action</th> -->
                                                        <th class="border-top-0">Action</th>
                                                        <th class="border-top-0">Status</th>
                                                    </tr>
                                                </thead>
                                                <?php $i = 1; ?>

                                                <tbody>
                                                    @foreach($allUsers as $users)
                                                    <tr>
                                                        <td class="align-middle">{{$i}}</td>
                                                        <td class="align-middle">{{$users->id}}</td>
                                                        <!-- <td class="align-middle"><a href="{{url('user_mgmt_view/'.$users->id)}}">{{$users->name}}</a></td> -->
                                                        <td class="align-middle">{{$users->name}}</td>
                                                        <td class="align-middle">{{$users->phone ?: '-'}}</td>
                                                        <td class="align-middle">{{$users->email}}</td>
                                                        <td>
                                                            <a href="{{url('/user_view/'.$users->id)}}"
                                                                data-placement="top" title="View">
                                                                <button class="btn btn-info btn-sm">View</button>
                                                            </a>
                                                        </td>
                                                        <td class="align-middle">
                                                            @if($users->status==1)
                                                            <label class="switch">
                                                                <input type="checkbox" class="statusToogle"
                                                                    data-status="0" data-id="{{$users->id}}" checked>
                                                                <span class="slider round"></span>
                                                            </label>
                                        </div>
                                        @else
                                        <label class="switch">
                                            <input type="checkbox" class="statusToogle" data-status="1"
                                                data-id="{{$users->id}}">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    @endif </td>
                                    </tr>
                                    <?php ++$i; ?>
                                    @endforeach
                                    </tbody>

                                    <tfoot>
                                        <tr>

                                        </tr>
                                    </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- **// main content -->

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

<script>
$(document).on('click', '.statusToogle', function() {
    var status = $(this).attr('data-status');
    var id = $(this).attr('data-id');
    console.log("id=" + id);
    console.log("status=" + status);
    //  alert('status===='+id);         

    $.ajax({
        type: "POST",
        url: "{{url('userStatus')}}",
        data: {
            "_token": "{{ csrf_token() }}",
            "status": status,
            "id": id
        },
        success: function(data) {
            console.log("=====" + data);
            if (status == 1) {
                $(this).attr('data-status', 0);

                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function() {
                    x.className = x.className.replace("show", "");
                }, 3000);
                location.reload();
            } else {
                $(this).attr('data-status', 1);
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function() {
                    x.className = x.className.replace("show", "");
                }, 3000);
                //   alert("DISABLE");
                location.reload();
            }
            //  location.reload(); 
        }
    });

});



$(document).ready(function() {
    $('#myTable').DataTable();
});
</script>
@stop