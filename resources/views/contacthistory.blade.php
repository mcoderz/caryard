@extends('layouts.successlogin')
@section('content')
<section>
            <div id="page-content-wrapper" class=" mt-65">
                <div id="content">
                    <div class="container-fluid p-0 px-lg-0 px-md-0">

                        <!-- Begin Page Content -->
                        <div class="container-fluid px-lg-4">
                            <div class="row">
                                <div class="col-md-12 mt-lg-4 mt-4">
                                    <!-- Page Heading -->
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Contact History</h1>
                                        <a href="javascript:history.back()"
                                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"
                                            >
                                            <i class="fas fa-arrow-left fa-sm text-white-50" aria-hidden="true"></i>
                                            Back</a>
                                       
                                    </div>
                                </div>

                                <!-- column -->

                                <!-- Datatable -->
                                <div class="col-md-12 mb-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <!-- title -->
                                            <!-- <div class="d-md-flex align-items-center">
                                                <div>
                                                    <h4 class="card-title">DataTable</h4>
                                                    <h5 class="card-subtitle">Overview of Top Selling Items</h5>
                                                </div>
                                                <div class="ml-auto">
                                                    <div class="dl">
                                                        <select class="custom-select">
                                                            <option value="0" selected="">Monthly</option>
                                                            <option value="1">Daily</option>
                                                            <option value="2">Weekly</option>
                                                            <option value="3">Yearly</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- title -->
                                        
                                        <div>
                                            <div class="table-responsive">
                                                <table class="table v-middle" id="datatable">
                                                    <thead>
                                                        <tr class="bg-light">
                                                            <th class="border-top-0" width="50">S.N.</th>
                                                            <th class="border-top-0">User Name</th>
                                                            <th class="border-top-0">Admin Name</th>
                                                            <th class="border-top-0" width="250">Description</th>
                                                            <th class="border-top-0">Date</th>
                                                            <!-- <th class="border-top-0">Status</th> -->
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="m-r-10"><a
                                                                            class="btn btn-circle btn-info text-white">EA</a>
                                                                    </div>
                                                                    <div class="">
                                                                        <h4 class="m-b-0 font-16">Elite Admin</h4>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="m-r-10"><a
                                                                            class="btn btn-circle btn-info text-white">EA</a>
                                                                    </div>
                                                                    <div class="">
                                                                        <h4 class="m-b-0 font-16">Elite Admin</h4>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        
                                                            <td> Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi fugiat, iste illum magnam nisi laborum debitis earum praesentium soluta ipsa, iusto adipisci velit error dignissimos nesciunt impedit quaerat tempore eligendi! </td>
                                                            <td>21-02-2021</td>
                                                            <!-- <td>
                                                            <div class="button-switch">
                                                                    <input type="checkbox" id="switch-1"
                                                                        class="switch" />
                                                                    <label for="switch-1" class="lbl-off">Off</label>
                                                                    <label for="switch-1" class="lbl-on">On</label>
                                                                </div>
                                                            </td> -->
                                                           
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td>2</td>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="m-r-10"><a
                                                                            class="btn btn-circle btn-info text-white">EA</a>
                                                                    </div>
                                                                    <div class="">
                                                                        <h4 class="m-b-0 font-16">Elite Admin</h4>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="m-r-10"><a
                                                                            class="btn btn-circle btn-info text-white">EA</a>
                                                                    </div>
                                                                    <div class="">
                                                                        <h4 class="m-b-0 font-16">Elite Admin</h4>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        
                                                            <td> Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi fugiat, iste illum magnam nisi laborum debitis earum praesentium soluta ipsa, iusto adipisci velit error dignissimos nesciunt impedit quaerat tempore eligendi! </td>
                                                            <td>21-02-2021</td>
                                                            <!-- <td>
                                                            <div class="button-switch">
                                                                    <input type="checkbox" id="switch-2"
                                                                        class="switch" />
                                                                    <label for="switch-2" class="lbl-off">Off</label>
                                                                    <label for="switch-2" class="lbl-on">On</label>
                                                                </div>
                                                            </td>
                                                            -->
                                                        </tr>
                                                        
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Datatable -->

                            </div>
                            <!-- Table and Form Insert here -->

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>

            </div>
            </div>
        </section>
        <!-- /#page-content-wrapper -->

        <!-- Add Contact Popup -->
        <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer justity-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Save </button>
      </div>
    </div>
  </div>
</div>