<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700;800;900&display=swap" rel="stylesheet">
    <title>Admin Login</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&family=Roboto:wght@300;400;500;700;900&display=swap"
        rel="stylesheet">
    <!--fontawesome-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="assets/css/main-style.css">
    <style>
        
    </style>
</head>

<body>

    <div class="admin-login__wrapper admin-login">
        <div class="admin-login__content">
          <div class="admin-login__main-content container-fluid">
            <div class="admin-login__container">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header bg-transparent border-bottom-0">
                    <img class="logo-img" src="assets/images/mobilecoderz-logo.svg" alt="logo" width="102" height="27">
                    <span class="admin-login__description">Please enter your user information.                    </span></div>
                <div class="card-body">
                    <form action="index.html" method="get">
                        <span class="admin-login__title pb-4">Sign Up</span>
                        <div class="form-group">
                          <input class="form-control" type="text" name="nick" required="" placeholder="Username" autocomplete="off">
                        </div>
                        <div class="form-group">
                          <input class="form-control" type="email" name="email" required="" placeholder="E-mail" autocomplete="off">
                        </div>
                        <div class="form-group row signup-password">
                          <div class="col-6">
                            <input class="form-control" id="pass1" type="password" required="" placeholder="Password">
                          </div>
                          <div class="col-6">
                            <input class="form-control" required="" placeholder="Confirm">
                          </div>
                        </div>
                        <div class="form-group pt-2">
                          <!-- <button class="btn btn-block btn-primary btn-xl py-1" type="submit">Sign Up</button> -->
                        </div>
                        <div class="title">
                            <span class="admin-login__title pb-3">Or</span>
                        </div>
                        <div class="form-group row social-signup pt-0">
                          <div class="col-6">
                            <button class="btn btn-lg btn-block btn-social btn-facebook btn-color" type="button">
                                 Facebook</button>
                          </div>
                          <div class="col-6">
                            <button class="btn btn-lg btn-block btn-social btn-google-plus btn-color" type="button">
                                Google Plus</button>
                          </div>
                        </div>
                        <div class="form-group pt-3 mb-3">
                          <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="check1">
                            <label class="custom-control-label" for="check1">By creating an account, you agree the <a href="#">terms and conditions</a>.</label>
                          </div>
                        </div>
                      </form>
                </div>
              </div>
              <div class="admin-login__footer text-center font-15">
                  <span>© 2020 Company Name</span></div>
            </div>
          </div>
        </div>
      </div>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/jquery-3.0.0.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custome-script.js"></script>
    
</body>

</html>