@extends('layouts.successlogin')
@section('content')
<div id="content">
<section>
            <div id="page-content-wrapper" class=" mt-65">
                <div id="content">
                    <div class="container-fluid p-0 px-lg-0 px-md-0">

                        <!-- Begin Page Content -->
                        <div class="container-fluid px-lg-4">
                            <div class="row">
                                <div class="col-md-12 mt-lg-4 mt-4">
                                    <!-- Page Heading -->
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                                        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                                            class="fas fa-download fa-sm text-white-50"></i>
                                        Generate Report</a> -->
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <!-- Card -->
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title mb-4">Listed Cars</h5>
                                                    <div class="row align-items-center">                                                        
                                                        <div class="col-lg-6">
                                                            <div class="mb-1 d-flex justify-content-between"> 
                                                                <span class="text-muted">Day</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['daycount']}} 
                                                                </span>
                                                            </div>
                                                            <div class="mb-1 d-flex justify-content-between">
                                                                <span class="text-muted">Week</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['weekcount']}} 
                                                                </span>
                                                            </div>
                                                            <div class="mb-1 d-flex justify-content-between">
                                                                <span class="text-muted">Month</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['monthcount']}}  
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h4 class="text-center text-uppercase font-weight-bold text-success">Total</h4>
                                                            <h4 class=" mt-1 mb-3 text-center  text-success">{{$data['totalcard']}} </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                        <!-- Card -->
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title mb-4">Sold Cars</h5>
                                                    <div class="row align-items-center">                                                        
                                                        <div class="col-lg-6">
                                                            <div class="mb-1 d-flex justify-content-between"> 
                                                                <span class="text-muted">Day</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['daysold']}}  
                                                                </span>
                                                            </div>
                                                            <div class="mb-1 d-flex justify-content-between">
                                                                <span class="text-muted">Week</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['weeksold']}}  
                                                                </span>
                                                            </div>
                                                            <div class="mb-1 d-flex justify-content-between">
                                                                <span class="text-muted">Month</span>
                                                                <span class="text-danger"> 
                                                                    <i class="mdi mdi-arrow-bottom-right"></i> {{$data['monthdold']}}  
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h4 class="text-center text-uppercase font-weight-bold text-success">Total</h4>
                                                            <h4 class=" mt-1 mb-3 text-center  text-success">{{$data['totalsold']}}  </h4>
                                                        </div>
                                                    </div>
                                                    <!-- <h1 class="display-5 mt-1 mb-3">50</h1>
                                                    <div class="mb-1">
                                                        <span class="text-danger"> <i
                                                            class="mdi mdi-arrow-bottom-right"></i> +3.65% </span>
                                                        <span class="text-muted">Sold last week</span>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                        <!-- Card -->
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title mb-4">Profits</h5>
                                                    <h1 class="display-5 mt-1 mb-3">{{$data['allusers']}} </h1>
                                                    <div class="mb-1">
                                                        <span class="text-danger"> <i
                                                            class="mdi mdi-arrow-bottom-right"></i> +2.00% </span>
                                                        <span class="text-muted">Profit last week</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                       
                                        <!-- Card -->
                                    </div>
                                </div>
                                <!-- column -->
                                
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </div>

                </div>
            </div>
        </section>
@stop