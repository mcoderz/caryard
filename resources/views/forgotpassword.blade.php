<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="{{url('../assets/css/bootstrap.min.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700;800;900&display=swap" rel="stylesheet">
    <title>Admin Login</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&family=Roboto:wght@300;400;500;700;900&display=swap"
        rel="stylesheet">
    <!--fontawesome-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <!-- <link rel="stylesheet" href="assets/css/main-style.css"> -->
    <link rel="stylesheet" href="{{url('../assets/css/main-style.css')}}" />
    <style>
        
    </style>
</head>

<body>

    <div class="admin-login__wrapper admin-login">
        <div class="admin-login__content">
          <div class="admin-login__main-content container-fluid">
            <div class="admin-login__container">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header bg-transparent border-bottom-0">
                    <img class="logo-img" src="{{url('assets/images/mobilecoderz-logo.svg')}}" alt="logo" width="102" height="27">
                    <span class="admin-login__description">Forgot your password?</span></div>
                <div class="card-body">
                    <form>
                        <p class="font-13">Don't worry, we'll send you an email to reset your password.</p>
                        <div class="form-group pt-4">
                          <input class="form-control" type="email" name="email" required="" placeholder="Your Email" autocomplete="off">
                        </div>
                        <p class="pt-1 pb-4 font-13">Don't remember your email? <a href="#">Contact Support</a>.</p>
                        <div class="form-group pt-1"><a class="btn btn-block btn-primary btn-xl py-1" href="index.html">Reset Password</a></div>
                      </form>
                </div>
              </div>
              <div class="admin-login__footer text-center font-15">
                  <span>© 2020 Company Name</span></div>
            </div>
          </div>
        </div>
      </div>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="assets/js/jquery-3.0.0.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custome-script.js"></script> -->
    <script src="{{url('../assets/js/jquery-3.0.0.min.js')}}"></script>
	  <script src="{{url('../assets/js/popper.min.js')}}"></script>
    <script src="{{url('../assets/js/bootstrap.min.js')}}"></script>
    <script src="{{url('../assets/js/custome-script.js')}}"></script>
</body>

</html>