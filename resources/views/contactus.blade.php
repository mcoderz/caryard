@extends('layouts.successlogin')
@section('content')
<section>
    <div id="page-content-wrapper" class=" mt-65">
        <div id="content">
            <div class="container-fluid p-0 px-lg-0 px-md-0">

                <!-- Begin Page Content -->
                <div class="container-fluid px-lg-4">
                    <div class="row">
                        <div class="col-md-12 mt-lg-4 mt-4">
                            <!-- Page Heading -->
                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 class="h3 mb-0 text-gray-800">Contact Us</h1>

                            </div>
                        </div>

                        <!-- column -->

                        <!-- Datatable -->
                        <div class="col-md-12 mb-4">
                            <div class="card">
                                <div class="card-body">

                                    <div>
                                        <div class="table-responsive">
                                            <table class="table v-middle" id="datatable">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="border-top-0">S.No.</th>
                                                        <th class="border-top-0">User Name</th>
                                                        <!-- <th class="border-top-0">Phone Number</th> -->
                                                        <th class="border-top-0">Email ID</th>
                                                        <th class="border-top-0" width="250">Description</th>
                                                        <th class="border-top-0">Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1; ?>
                                                    @foreach($allcomplaints as $allcomplaint)

                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$allcomplaint->name}}</td>
                                                        <td>{{$allcomplaint->email}}</td>
                                                        <td>{{$allcomplaint->complaint}}</td>

                                                        <td>
                                                            <ul
                                                                class="list-unstyled d-flex justify-content-center align-content-center">
                                                                <li class="px-1">
                                                                    <a data-toggle="modal" data-target="#addPlan" data-id="{{$allcomplaint->id}}" data-email="{{$allcomplaint->email}}"class="btn btn-info btn-circle-custome reply">
                                                                        <i class="fas fa-reply"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>

                                                    </tr>

                                                    <?php ++$i; ?>
                                                    @endforeach
                                                </tbody>


                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Datatable -->
                    </div>
                    <!-- Table and Form Insert here -->
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>

    </div>
    </div>
</section>
<!-- /#page-content-wrapper -->

<!-- Add Contact Popup -->
<!-- Modal -->
<div class="modal fade" id="addPlan"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url('/admin/replyToUser') }}">
                  {{ csrf_field() }}
                  <input type="hidden" id='id' name="id">
                  <input type="hidden" id='email' name="email">

                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right" for="planDuration">Reply</label>
                        <div class="col-12 col-sm-8 col-lg-8">
                        <textarea type="textarea" name="reply" class="form-control" id="planDuration" placeholder="Reply to user" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send </button>
                </div>

            </form>
        </div>
    </div>
</div>
@stop
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).on('click','.reply',function(){
        var id = $(this).attr('data-id');
        var email = $(this).attr('data-email');
        // alert(email)
        $('#id').val(id)
        $('#email').val(email)
        $('#exampleModalLabel').html('Admin Reply to '+ email);
    })
</script>