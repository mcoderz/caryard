<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'device_type' => 'required|in:android,ios',
            'device_token' => 'required',
            'device_id' => 'required',
            'phone' => 'required|string|min:10|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|max:20|same:password',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $User = $request->all();
            $User['password'] = bcrypt($request->password);
            $User['user_code'] = mt_rand(10000000, 99999999);

            $User['newUser'] = 1;

            $data = User::Create($User);

            $credentials = ['email' => $data->email, 'password' => $request->password];

            if (!auth()->attempt($credentials)) {
                return response(['status' => 0, 'msg' => 'Invalid Credentials'], 500);
            }

            $accessToken = $data->createToken($request->device_type)->plainTextToken;

            return response([
                'msg' => 'You have successfully account created.',
                'token_type' => 'Bearer',
                'access_token' => $accessToken,
                'data' => Auth::user(),
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
                 ], 500);
        }
    }
}
