<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CarBody;
use App\Models\CarBrand;
use App\Models\CarModels;
use App\Models\Cars;
use App\Models\CarsImage;
use App\Models\Cylinders;
use App\Models\ExteriorColor;
use App\Models\FavoritCars;
use App\Models\InteriorColor;
use App\Models\Transmission;
use Auth;
use Illuminate\Http\Request;

class CarBrandController extends Controller
{
    public function getAllCarBrand()
    {
        $car = CarBrand::all();

        return response([
        'message' => 'successfully',
        'carBrand' => $car,
        ], 200);
    }

    public function getCarModels(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $models = CarModels::where('car_brand_id', $request->brand_id)->get();

            return response([
            'message' => 'successfully',
            'carModels' => $models,
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function carSpecifications(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'model_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $cylinders = Cylinders::where('car_models_id', $request->model_id)->get();
            $transmission = Transmission::where('car_models_id', $request->model_id)->get();
            $car_body = CarBody::where('car_models_id', $request->model_id)->get();
            $exterior_color = ExteriorColor::where('car_models_id', $request->model_id)->get();
            $interior_color = InteriorColor::where('car_models_id', $request->model_id)->get();

            return response([
            'message' => 'successfully',
            'cylinders' => $cylinders,
            'transmission' => $transmission,
            'body_type' => $car_body,
            'exterior_color' => $exterior_color,
            'interior_color' => $interior_color,
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function addcar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
            'model_id' => 'required|numeric',
            'year' => 'required',
            'import' => 'required',
            'price' => 'required',
            'mileage' => 'required',
            'cylinders_id' => 'required|numeric',
            'transmission_id' => 'required|numeric',
            'body_type_id' => 'required|numeric',
            'exterior_color_id' => 'required|numeric',
            'interior_color_id' => 'required|numeric',
            'description' => 'required',
            'photos' => 'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $obj = [
                    'user_id' => Auth::id(),
                    'brand_id' => $request->brand_id,
                    'model_id' => $request->model_id,
                    'year' => $request->year,
                    'import' => $request->import,
                    'price' => $request->price,
                    'mileage' => $request->mileage,
                    'cylinders_id' => $request->cylinders_id,
                    'transmission_id' => $request->transmission_id,
                    'body_type_id' => $request->body_type_id,
                    'exterior_color_id' => $request->exterior_color_id,
                    'interior_color_id' => $request->interior_color_id,
                    'description' => $request->description,
            ];

            $carid = Cars::insertGetId($obj);
            $imageid = [];
            foreach ($request->photos as $image) {
                $image_name = time().$image->getClientOriginalName();
                $image->move(public_path('images/cars'), $image_name);
                $image_url = '/images/cars/'.$image_name;
                $getId = CarsImage::insertGetId(['car_id' => $carid, 'image' => $image_url]);
                array_push($imageid, $getId);
            }
            $cardata = CarsImage::whereIn('id', $imageid)->get();

            $obj['carImages'] = $cardata;

            return response([
                'message' => 'successfully',
                'addcar' => $obj,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function allCars(Request $request)
    {
        try {
            $cars = Cars::getallcar();
            $id = Auth::id();
            foreach ($cars as $car) {
                $user = FavoritCars::where('user_id', $id)->where('car_id', $car->id)->first();
                if ($user) {
                    $car->isfavourit = '1';
                } else {
                    $car->isfavourit = '0';
                }

                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'all_cars' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function filterCars(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
            'model_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $cars = Cars::getallcarfilter($request->brand_id, $request->model_id);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'filters_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function favoriteCar(Request $request)
    {
        try {
            $id = Auth::id();
            $uders = FavoritCars::where('user_id', $id)->get();
            $newcarid = [];
            foreach ($uders as $uder) {
                array_push($newcarid, $uder->car_id);
            }
            $cars = Cars::getallcars($newcarid);
            // print_r($cars); die;
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'favorites_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function favoriteMark(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $id = Auth::id();
            $uders = FavoritCars::insert(['car_id' => $request->car_id, 'user_id' => $id]);

            return response([
                'message' => 'Favorite mark successfully',
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function usercardetail(Request $request)
    {
        try {
            $id = Auth::id();
            $cars = Cars::getcardetail($id);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'Usercar' => $cars,
            ], 200);
        } catch (\Exception $e) {
            return response([
            // 'error'=>$e->getMessage(),
            'msg' => 'Something went wrong',
              ], 500);
        }
    }

    public function deleteusercar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'msg' => $validator->errors()->first(),
            ], 400);
            }
            $id = Auth::id();
            $car_id = $request->car_id;
            Cars::where('user_id', $id)->where('id', $car_id)->delete();
            CarsImage::where('car_id', $car_id)->delete();

            return response([
                    'message' => 'delete successfully',
                 ], 200);
        } catch (\Exception $e) {
            return response([
            // 'error'=>$e->getMessage(),
          'msg' => 'Something went wrong',
          ], 500);
        }
    }

    public function editusercar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'msg' => $validator->errors()->first(),
            ], 400);
            }
            $id = Auth::id();
            $car_id = $request->car_id;

            $car_id = Cars::find($car_id);
            $car_id->brand_id = $request->get('brand_id');
            $car_id->model_id = $request->get('model_id');
            $car_id->year = $request->get('year');
            $car_id->import = $request->get('import');
            $car_id->price = $request->get('price');
            $car_id->mileage = $request->get('mileage');
            $car_id->cylinders_id = $request->get('cylinders_id');
            $car_id->transmission_id = $request->get('transmission_id');
            $car_id->body_type_id = $request->get('body_type_id');
            $car_id->exterior_color_id = $request->get('exterior_color_id');
            $car_id->interior_color_id = $request->get('interior_color_id');
            $car_id->description = $request->get('description');
            $car_id->save();

            return response([
                'message' => 'successfully',
                'editcar' => $car_id,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function markassold(Request $request)
    {
        try {
            $id = Auth::id();
            $car_id = $request->car_id;
            print_r($car_id);
            die();

            $car = Cars::where('user_id', $id)->where('id', $car_id)->first();

            if ($car) {
                Cars::where('user_id', $id)->where('id', $car_id)->update(['status' => $request->status]);

                return response([
            // 'error'=>$e->getMessage(),
            'msg' => 'sucessfully',
        ], 200);
            } else {
                return response([
        // 'error'=>$e->getMessage(),
        'msg' => 'user not exists',
    ], 500);
            }
        } catch (\Exception $e) {
            return response([
        // 'error'=>$e->getMessage(),
        'msg' => 'Something went wrong',
    ], 500);
        }

//     if($car_id->status="1")
//     {
//      $car="active";
//     }
// else{
//       $car->isfavourit="sold";
//     }
    }

    public function carDetails(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'msg' => $validator->errors()->first(),
            ], 400);
            }

            $cars = Cars::getcardata($request->car_id);
            $id = Auth::id();
            foreach ($cars as $car) {
                $user = FavoritCars::where('user_id', $id)->where('car_id', $car->id)->first();
                if ($user) {
                    $car->isfavourit = '1';
                } else {
                    $car->isfavourit = '0';
                }
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'filters_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }
}
