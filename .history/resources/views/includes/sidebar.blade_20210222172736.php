
 <section>
            <nav class="fixed-top align-top" id="sidebar-wrapper" role="navigation">
                <div class="simplebar-content" style="padding: 0px;">
                    <a class="sidebar-brand" href="{{url('admin/dashboard')}}">
                        <!-- <span class="align-middle">Admin Template</span> -->
                        <img src="{{url('../assets/images/logo.png')}}" alt="logo" class="img-fluid align-middle" style="height:40px;" />
                    </a>

                    <div class="left-sidebar-scroll">
                        <div class="left-sidebar-content">
                            <ul id="sidebar_menu" class="navbar-nav align-self-stretch sidebar-elements">
                                <!-- <li class="sidebar-header">
                                Pages
                            </li> -->
                                <li class="">
                                    <a href="{{url('admin/dashboard')}}" class="nav-link text-left" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-tachometer-alt"></i> Dashboard
                                    </a>
                                </li>

                                <!-- <li class="has-sub">
                                    <a class="nav-link collapsed text-left" href="#collapseExample2" role="button" data-toggle="collapse">
                                        <i class="far fa-file-alt"></i> Pages
                                    </a>
                                    <div class="collapse menu mega-dropdown" id="collapseExample2">
                                        <div class="dropmenu" aria-labelledby="navbarDropdown">
                                            <div class="container-fluid ">
                                                <div class="row">
                                                    <div class="col-lg-12 px-2">
                                                        <div class="submenu-box">
                                                            <ul class="list-unstyled m-0">
                                                                <li><a href="login.html">Login</a></li>
                                                                <li><a href="signup.html">Signup</a></li>
                                                                <li><a href="forget-password.html">Forget Password</a></li>
                                                                <li><a href="404.html">404 Page</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li> -->

                                <li class="">
                                    <a href="{{url('admin/userlist')}}" class="nav-link text-left" role="button">
                                        <i class="fas fa-users"></i> User
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{url('admin/planlist')}}" class="nav-link text-left" role="button">
                                    <i class="fas fa-tasks"></i> Subscribe Plan
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{url('admin/criteria')}}" class="nav-link text-left" role="button">
                                    <i class="fas fa-clipboard-check"></i> Criteria
                                    </a>
                                </li>
                                <li class="">
                                    <a href="{{url('admin/newcar')}}" class="nav-link text-left" role="button">
                                    <i class="fas fa-car"></i> New Car
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{url('admin/contactus')}}" class="nav-link text-left" role="button">
                                    <i class="far fa-address-book"></i> Contact Us
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{url('admin/blankpage')}}" class="nav-link text-left" role="button">
                                        <i class="fas fa-file"></i> Blank page
                                    </a>
                                </li>
                                

                               

                            </ul>
                        </div>
                    </div>

                </div>
            </nav>
        </section>
        