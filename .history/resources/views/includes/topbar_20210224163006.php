 <!-- Topbar -->
 <section>
            <nav id="top-header" class="navbar navbar-expand navbar-light fixed-top my-navbar">
                <!-- Sidebar Toggle (Topbar) -->
                <div type="button" id="bar" class="nav-icon1 hamburger animated fadeInLeft is-closed" data-toggle="offcanvas">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <ul class="navbar-nav ml-auto align-items-center">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Shiv Design</span>
                            <img class="img-profile rounded-circle" src="{{url('../assets/images/logo3.png')}}">
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right py-0 rounded-0" aria-labelledby="alertsDropdown">
                            <div class="dropdown-menu-header">
                                <a class="dropdown-item" href="#"><i class="far fa-user-circle"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog"></i>Settings</a>
                                <a class="dropdown-item" ><i class="fas fa-sign-out-alt"></i>Logout</a>
                            </div>

                        </div>
                    </li>
                </ul>
            </nav>
            <div class="modal fade" id="logoutModel">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0">
          <h4 class="modal-title">Are you sure?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body text-center">
          <input type="hidden" id="idDelete" >
          <a type="button" href="logout" class="btn btn-danger logoutadmin">Logout</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
        
      </div>
    </div>
  </div>
<!-- end Delete -->


        </section>
        
        <!-- End of Topbar --><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>
  $(document).on('click','.dropdown-item',function(){
    $('#logoutModel').modal('show');
  });
</script>