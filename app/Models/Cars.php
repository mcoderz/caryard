<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Laravel\Jetstream\Events\TeamCreated;
use Laravel\Jetstream\Events\TeamDeleted;
use Laravel\Jetstream\Events\TeamUpdated;

class Cars extends Model
{
    protected $table = 'cars';

    public static function getallcar()
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'cars.status', 'cars.created_at', 'users.name as username', 'users.email', 'users.phone','cars.approved_by_supplier','cars.timely_maintenance','cars.vn_number')
                    ->paginate(10);

        return $data;
    }

    public static function getcardetail($id)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->where('cars.user_id', $id)
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'cars.status', 'cars.created_at', 'users.name as username', 'users.email', 'users.phone','publish_status','cars.approved_by_supplier','cars.timely_maintenance','cars.vn_number')
                    ->get();

        return $data;
    }

    public static function getallcarfilter($bid, $mid)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'users.name as username', 'users.email', 'users.phone', 'cars.status')
                    ->where('cars.brand_id', $bid)
                    ->where('cars.model_id', $mid)
                    ->get();

        return $data;
    }

    public static function getusersearch($bname)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'users.name as username', 'users.email', 'users.phone', 'cars.status')
                    ->where('brand_name', $bname)
                    ->get();

        return $data;
    }

    public static function getallcars($ids)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'users.name as username', 'users.email', 'users.phone', 'cars.status','cars.approved_by_supplier','cars.timely_maintenance','cars.vn_number')
                    ->whereIn('cars.id', $ids)
                    ->get();

        return $data;
    }

    public static function getcardata($id)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'users.name as username', 'users.email', 'users.phone', 'cars.status','cars.brand_id','cars.model_id','cars.cylinders_id','cars.transmission_id','cars.body_type_id','cars.exterior_color_id','cars.interior_color_id','cars.approved_by_supplier','cars.timely_maintenance','cars.vn_number')
                    ->where('cars.id', $id)
                    ->get();

        return $data;
    }

    public static function getsearch($bname, $mname, $year, $color)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'users.name as username', 'users.email', 'users.phone', 'cars.status')
                    ->where('car_brand.brand_name', $bname)
                    ->where('car_models.model_name', $mname)
                    ->where('cars.year', $year)
                    ->where('exterior_color.ext_color', $color)
                    ->get();

        return $data;
    }

    public static function comparetwocars($id)
    {
        $data = DB::table('cars')
                    ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                    ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                    ->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')
                    ->join('transmission', 'cars.transmission_id', '=', 'transmission.id')
                    ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                    ->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')
                    ->join('interior_color', 'cars.interior_color_id', '=', 'interior_color_id')
                    ->join('users', 'cars.user_id', '=', 'users.id')
                    ->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'car_body.body_name', 'transmission.transmission_name', 'exterior_color.ext_color', 'interior_color.int_color', 'cars.status', 'cars.created_at', 'users.name as username', 'users.email', 'users.phone')
                    // ->whereIn('cars.id',[2,3])
                    ->whereIn('cars.id', $id)
                    ->get();

        return $data;
    }

    protected $dispatchesEvents = [
        'created' => TeamCreated::class,
        'updated' => TeamUpdated::class,
        'deleted' => TeamDeleted::class,
    ];
}
