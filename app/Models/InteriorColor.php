<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InteriorColor extends Model
{
    protected $table = "interior_color";




    protected $dispatchesEvents = [
       'created' => TeamCreated::class,
       'updated' => TeamUpdated::class,
       'deleted' => TeamDeleted::class,
   ];
}
