<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoritCars extends Model
{
    protected $table = "favorite_cars";

    protected $dispatchesEvents = [
       'created' => TeamCreated::class,
       'updated' => TeamUpdated::class,
       'deleted' => TeamDeleted::class,
   ];
}
