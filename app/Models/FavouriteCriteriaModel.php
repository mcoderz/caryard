<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouriteCriteriaModel extends Model
{
    use HasFactory;

    protected $table = 'favourite_criteria';
}
