<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usersubscribes extends Model
{
    use HasFactory;
    protected $table ='Usersubscribes';
}
