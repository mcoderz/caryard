<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarBody extends Model
{
    protected $table = "car_body";




    protected $dispatchesEvents = [
       'created' => TeamCreated::class,
       'updated' => TeamUpdated::class,
       'deleted' => TeamDeleted::class,
   ];
}
