<?php

namespace App\Http\Controllers;

use App\Models\ContactUsModel;
use Symfony\Component\HttpFoundation\Request;
use SendGrid\Mail\From as From;
use SendGrid\Mail\Mail as Mail;
use SendGrid\Mail\To as To;
class ContactUsController extends Controller
{

    public function contactus()
    {
        $allcomplaints = ContactUsModel::join('users', 'contactUs.user_id', '=', 'users.id')->orderby('contactUs.id', 'desc')->select('contactUs.id', 'contactUs.user_id', 'contactUs.complaint', 'users.name', 'users.email')->get();

        return view('contactus', compact('allcomplaints'));
    }

    public function replyToUser(Request $request){                                                                                                              ContactUsModel::where('id',$request->id)->update(['reply'=>$request->reply]);
        try {
            $email = $request->email;
            $name = 'Car Market';
            $from = new from('algharabally360@gmail.com', 'Car Market');
            $tos = [new To(
            $email,
            $name,
            [
            'subject' => 'Reply To Complaint.',
            'replyMsg' => $request->reply,
            ])];
            $email = new Mail($from, $tos);
            $email->setTemplateId('d-8eee6319901e4266bb610543dc53dd9c');
            $sendgrid = new \SendGrid('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');
            try {
                $response = $sendgrid->send($email);
                $returndata = 'success';
            } catch (Exception $e) {
                $returndata = 'failes';
            }
        } catch (Exception $ex) {
        }
    return back();
        
    }
}