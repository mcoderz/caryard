<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function invoice()
    {
        $datas = DB::table('Usersubscribes')
                    ->join('SubscribePlans','Usersubscribes.subscribes_id','=','SubscribePlans.id')
                    ->join('users','Usersubscribes.user_id','=','users.id')
                    ->select('users.name','users.email','SubscribePlans.plan_name','SubscribePlans.amount')
                    ->get();

        return view('invoice',compact('datas'));
    }
}
