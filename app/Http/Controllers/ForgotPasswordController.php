<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Validator;

use App\Models\User;

use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    public function showResetForm($timeStamp, $id){

        // echo "dfd"; die;
        $user_id = base64_decode($id);
        // print_r($user_id); die;
        $linkTimeStamp = $timeStamp;
        $current_timestamp = Carbon::now()->timestamp;
        $now  = Carbon::now();
        $end  = Carbon::createFromTimestamp($linkTimeStamp)->toDateTimeString();
        $interval = $now->diffInHours($end);
        return view('resetpassword',compact('user_id'));
      }

      public function resetPassword(Request $request){

        // $v= Validator::make($request->all(), [
        //     'password'=>'required|min:6',
        //     'conformPassword'=>'required|same:new_password',
        //     ],
        //     [
        //     'new_password.required'=>'Please Enter new password',
        //     'new_password.min'=>'Password should be atleast 6 character.',
        //     'confirm_password.required'=>'Please Enter confirm  password',
        //     'confirm_password.same'=>'Password and confirm password should be same.',
        //     ]);
        //     if($v->fails()) {
        //         return redirect()->back()->withErrors($v);
        //     }
        extract($_POST);
        // print_r($_POST); die;
        $password = bcrypt($_POST['password']);
        $id = base64_decode($_POST['id']);
        // print_r($id); die;
        User::where('id',$id)->update(['password'=> $password]);
        session()
        ->flash('change','password Change Successfully');
          return back();
      }
}
