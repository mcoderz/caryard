<?php

namespace App\Http\Controllers;

use App\Models\CarBody;
use App\Models\CarBrand;
use App\Models\CarModels;
use App\Models\Cylinders;
use App\Models\ExteriorColor;
use App\Models\FavouriteCriteriaModel;
use App\Models\Transmission;
use App\Models\Userlist;
use Illuminate\Support\Facades\DB;

class FavoriteCriteriaController extends Controller
{
    public function criteria()
    {
        $allUsers = Userlist::orderBy('id', 'DESC')->get();

        return view('criteria', compact('allUsers'));
    }

    public function viewfavoritecriteria($id)
    {
        $allcars = FavouriteCriteriaModel::where('user_id', $id)->get();
        
        foreach( $allcars as  $allcar){

            if(!empty($allcar->brand_id)){
                $brand = CarBrand::where('id',$allcar->brand_id)->first();
                $allcar->brand_id =  $brand->brand_name;
            }
            if(!empty($allcar->model_id)){
                $model = CarModels::where('id',$allcar->model_id)->first();
                $allcar->model_id =  $model->model_name;
            }
            if(!empty($allcar->body_type_id)){
                $body = CarBody::where('id',$allcar->body_type_id)->first();
                $allcar->body_type_id =  $body->body_name;
            }
            if(!empty($allcar->exterior_color_id)){
                $extcolor = ExteriorColor::where('id',$allcar->exterior_color_id)->first();
                $allcar->exterior_color_id =  $extcolor->ext_color;
            }
            if(!empty($allcar->interior_color_id)){
                $intcolor = ExteriorColor::where('id',$allcar->interior_color_id)->first();
                $allcar->interior_color_id =  $intcolor->int_color;
            }
            if(!empty($allcar->cylinders_id)){
                $cyli = Cylinders::where('id',$allcar->cylinders_id)->first();
                $allcar->cylinders_id =  $cyli->cylinder;
            }
            if(!empty($allcar->transmission_id)){
                $tra = Transmission::where('id',$allcar->transmission_id)->first();
                $allcar->transmission_id =  $tra->transmission_name;
            }
        }
       
        return view('favoritecriteriaview', compact('allcars'));
    }
}
