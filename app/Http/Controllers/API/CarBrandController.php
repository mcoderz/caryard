<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CarBody;
use App\Models\CarBrand;
use App\Models\CarModels;
use App\Models\Cars;
use App\Models\CarsImage;
use App\Models\Cylinders;
use App\Models\ExteriorColor;
use App\Models\FavoritCars;
use App\Models\FavouriteCriteriaModel;
use App\Models\InteriorColor;
use App\Models\Notification;
use App\Models\SearchHistoryModel;
use App\Models\Transmission;
use App\Models\User;
use App\Models\Usersubscribes;
use Auth;
use DB;
use Illuminate\Http\Request;

class CarBrandController extends Controller
{
    public function getAllCarBrand()
    {
        $car = CarBrand::all();

        return response([
        'message' => 'successfully',
        'carBrand' => $car,
        ], 200);
    }

    public function getCarModels(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $models = CarModels::where('car_brand_id', $request->brand_id)->get();

            return response([
            'message' => 'successfully',
            'carModels' => $models,
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function carSpecifications(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'model_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $cylinders = Cylinders::where('car_models_id', $request->model_id)->get();
            $transmission = Transmission::where('car_models_id', $request->model_id)->get();
            $car_body = CarBody::where('car_models_id', $request->model_id)->get();
            $exterior_color = ExteriorColor::where('car_models_id', $request->model_id)->get();
            $interior_color = InteriorColor::where('car_models_id', $request->model_id)->get();

            return response([
            'message' => 'successfully',
            'cylinders' => $cylinders,
            'transmission' => $transmission,
            'body_type' => $car_body,
            'exterior_color' => $exterior_color,
            'interior_color' => $interior_color,
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function addcar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
            'model_id' => 'required|numeric',
            'year' => 'required',
            'import' => 'required',
            'price' => 'required',
            'mileage' => 'required',
            'cylinders_id' => 'required',
            'transmission_id' => 'required|numeric',
            'body_type_id' => 'required|numeric',
            'exterior_color_id' => 'required|numeric',
            'interior_color_id' => 'required|numeric',
            'description' => 'required',
            'photos' => 'required',
        ]);
         try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $subdata = Usersubscribes::where('user_id',Auth::id())->where('is_active',1)->first();
            if(empty($subdata)){
                return response([
                    'message' => 'Subscriptions Expired',
                    ], 400);
            }
            $obj = [
                    'user_id' => Auth::id(),
                    'brand_id' => $request->brand_id,
                    'model_id' => $request->model_id,
                    'year' => $request->year,
                    'import' => $request->import,
                    'price' => $request->price,
                    'mileage' => $request->mileage,
                    'cylinders_id' => $request->cylinders_id,
                    'transmission_id' => $request->transmission_id,
                    'body_type_id' => $request->body_type_id,
                    'exterior_color_id' => $request->exterior_color_id,
                    'interior_color_id' => $request->interior_color_id,
                    'description' => $request->description,
                    'approved_by_supplier' => $request->approved_by_supplier,
                    'timely_maintenance' => $request->timely_maintenance,
                    'vn_number' => $request->vn_number,
                    'user_sub_id'=>$subdata->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
            ];

            $carid = Cars::insertGetId($obj);
            $imageid = [];
            foreach ($request->photos as $image) {
                $image_name = time().$image->getClientOriginalName();
                $image->move(public_path('images/cars'), $image_name);
                $image_url = '/images/cars/'.$image_name;
                $getId = CarsImage::insertGetId(['car_id' => $carid, 'image' => $image_url]);
                array_push($imageid, $getId);
            }
            $cardata = CarsImage::whereIn('id', $imageid)->get();
            $obj['carImages'] = $cardata;

            $countcars = Cars::where('user_id',Auth::id())->where('user_sub_id',$subdata->id)->count();
            if( $countcars == $subdata->post_count){
            $subdata = Usersubscribes::where('user_id',Auth::id())->where('id',$subdata->id)->update(['is_active'=>0]);
            }

            $getfavcars = FavouriteCriteriaModel::where('brand_id',$request->brand_id)->where('model_id',$request->model_id)->where('year',$request->year)->where('price',$request->price)->where('import',$request->import)->where('mileage',$request->mileage)->get(); 

            if(count($getfavcars) > 0){
                foreach( $getfavcars  as  $getfavcar ){
                $this->pushNotification($getfavcar->user_id);
                }
            }
            return response([
                'message' => 'successfully',
                'addcar' => $obj,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function allCars(Request $request)
    {
        try {
        if (!empty($request->sort_by || $request->brand || $request->model || $request->year || $request->color)) {
            $data = Cars::join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                ->select('cars.id', 'cars.year', 'cars.price', 'car_models.model_name', 'car_brand.brand_name', 'cars.created_at', 'cars.approved_by_supplier', 'cars.timely_maintenance', 'cars.vn_number');
            if ($request->sort_by == 'LOW') {
                $data = $data->orderBy('cars.price', 'ASC');
            } elseif ($request->sort_by == 'HIGH') {
                $data = $data->orderBy('cars.price', 'DESC');
            } elseif ($request->sort_by == 'LATEST') {
                $data = $data->orderBy('cars.id', 'DESC');
            }

            if ($request->brand) {
                $data = $data->where('cars.brand_id', $request->brand);
            }
            if ($request->model) {
                $data = $data->where('cars.model_id', $request->model);
            }
            if ($request->year) {
                $data = $data->where('cars.year', $request->year);
            }
            if ($request->color) {
                $data = $data->where('cars.interior_color_id', $request->color);
            }
            $cars = $data->paginate(10);

            foreach ($cars as $datas) {
                $img = CarsImage::where('car_id', $datas->id)->get();
                $datas->model_image = $img;
            }
        } else {
            $cars = Cars::getallcar();
            $id = Auth::id();
            foreach ($cars as $car) {
                $user = FavoritCars::where('user_id', $id)->where('car_id', $car->id)->first();
                if ($user) {
                    $car->isfavourit = '1';
                } else {
                    $car->isfavourit = '0';
                }
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }
        }

        return response([
                'message' => 'successfully',
                'all_cars' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function filterCars(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_id' => 'required|numeric',
            'model_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $cars = Cars::getallcarfilter($request->brand_id, $request->model_id);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'filters_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function favoriteCar(Request $request)
    {
        try {
            $id = Auth::id();
            $uders = FavoritCars::where('user_id', $id)->get();
            $newcarid = [];
            foreach ($uders as $uder) {
                array_push($newcarid, $uder->car_id);
            }
            $cars = Cars::getallcars($newcarid);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'favorites_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function favoriteMark(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $id = Auth::id();
            $uders = FavoritCars::insert(['car_id' => $request->car_id, 'user_id' => $id]);

            return response([
                'message' => 'Favorite mark successfully',
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function usercardetail(Request $request)
    {
        try {
            $id = Auth::id();
            $cars = Cars::getcardetail($id);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'Usercar' => $cars,
            ], 200);
        } catch (\Exception $e) {
            return response([
            'message' => 'Something went wrong',
              ], 500);
        }
    }

    public function deleteusercar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->errors()->first(),
            ], 400);
            }
            $id = Auth::id();
            $car_id = $request->car_id;
            $getcar = Cars::where('user_id', $id)->where('id', $car_id)->first();
            if ($getcar) {
                Cars::where('user_id', $id)->where('id', $car_id)->delete();
                CarsImage::where('car_id', $car_id)->delete();

                return response([
                    'status' => 200,
                    'message' => 'delete successfully',
                 ], 200);
            } else {
                return response([
                    'status' => 400,
                    'message' => 'User not deleted this car.',
                 ], 400);
            }
        } catch (\Exception $e) {
            return response([
            // 'error'=>$e->getMessage(),
          'message' => 'Something went wrong',
          ], 500);
        }
    }

    public function editusercar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->errors()->first(),
            ], 400);
            }
            $id = Auth::id();
            // $carid =$request->car_id;
            $car_id = $request->car_id;
            $car_id = Cars::find($car_id);
            $car_id->brand_id = $request->get('brand_id');
            $car_id->model_id = $request->get('model_id');
            $car_id->year = $request->get('year');
            $car_id->import = $request->get('import');
            $car_id->price = $request->get('price');
            $car_id->mileage = $request->get('mileage');
            $car_id->cylinders_id = $request->get('cylinders_id');
            $car_id->transmission_id = $request->get('transmission_id');
            $car_id->body_type_id = $request->get('body_type_id');
            $car_id->exterior_color_id = $request->get('exterior_color_id');
            $car_id->interior_color_id = $request->get('interior_color_id');
            $car_id->description = $request->get('description');
            $car_id->approved_by_supplier = $request->get('approved_by_supplier');
            $car_id->timely_maintenance = $request->get('timely_maintenance');
            $car_id->vn_number = $request->get('vn_number');
            $car_id->save();
            // print_r($request->photos); die;
            if (!empty($request->photos)) {
                foreach ($request->photos as $image) {
                    // print_r($image); die;
                    $image_name = $image->getClientOriginalName();
                    $image->move(public_path('images/cars'), $image_name);
                    $image_url = '/images/cars/'.$image_name;
                    // print_r( $image_url ); die;

                    $getId = CarsImage::insert(['car_id' => $car_id['id'], 'image' => $image_url]);
                    // print_r( $getId ); die;
                }
            }

            return response([
                'message' => 'successfully',
                'editcar' => $car_id,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function markassold(Request $request)
    {
        try {
            $id = Auth::id();
            $car_id = $request->car_id;
            $car = Cars::where('user_id', $id)->where('id', $car_id)->first();

            if ($car) {
                Cars::where('user_id', $id)->where('id', $car_id)->update(['status' => $request->status]);

                return response([
            // 'error'=>$e->getMessage(),
            'message' => 'sucessfully',
        ], 200);
            } else {
                return response([
                    'message' => 'user not exists',
                ], 500);
            }
        } catch (\Exception $e) {
            return response([
            'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function carDetails(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->errors()->first(),
            ], 400);
            }

            $cars = Cars::getcardata($request->car_id);
            $id = Auth::id();
            foreach ($cars as $car) {
                $user = FavoritCars::where('user_id', $id)->where('car_id', $car->id)->first();
                if ($user) {
                    $car->isfavourit = '1';
                } else {
                    $car->isfavourit = '0';
                }
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'filters_car' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function usersearchcar(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
            ], 400);
        }
        try {
            $cars = Cars::getusersearch($request->brand_name);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
            'message' => 'successfully',
            'filters_car' => $cars,
            ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function usersearch(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'brand_name' => 'required|string',
            'model_name' => 'required|string',
            'year' => 'required|numeric',
            'ext_color' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
            ], 400);
        }
        try {
            $cars = Cars::getsearch($request->brand_name, $request->model_name, $request->year, $request->ext_color);
            foreach ($cars as $car) {
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
            'message' => 'successfully',
            'filters_car' => $cars,
            ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function unfavoriteMark(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            $id = Auth::id();
            $uders = FavoritCars::where(['car_id' => $request->car_id])->delete();

            return response([
                'message' => 'UnFavorite mark successfully',
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function sortBy(Request $request)
    {
        try {
            $data = Cars::query();
            if ($request->type == 'latest') {
                $data = $data->where('user_id', Auth::id())->orderBy('id', 'desc');
            }
            if ($request->type == 'low') {
                $data = $data->where('user_id', Auth::id())->orderBy('price', 'asc');
            }
            if ($request->type == 'high') {
                $data = $data->where('user_id', Auth::id())->orderBy('price', 'desc');
            }
            $data = $data->get();

            return response([
                'message' => ' successfully',
                'data' => $data,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function search_api(Request $request)
    {
        try {
            if ($request->data) {
                $getsearchdata = SearchHistoryModel::where('user_id', Auth::id())->where('searching', $request->data)->first();
                if (empty($getsearchdata)) {
                    SearchHistoryModel::insert(['user_id' => Auth::id(), 'searching' => $request->data]);
                }
            }
            $data = Cars::join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                        ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                        ->join('car_body', 'cars.body_type_id', '=', 'car_body.id')
                        ->where('car_brand.brand_name', 'LIKE', '%'.$request->data.'%')
                        ->orwhere('car_models.model_name', 'LIKE', '%'.$request->data.'%')
                        ->orwhere('car_body.body_name', 'LIKE', '%'.$request->data.'%')
                        ->select('cars.id', 'cars.year', 'cars.price', 'car_models.model_name', 'car_brand.brand_name', 'cars.created_at','cars.approved_by_supplier', 'cars.timely_maintenance', 'cars.vn_number')
                        ->get();
            foreach ($data as $datas) {
                $img = CarsImage::where('car_id', $datas->id)->first();
                if ($img) {
                    $datas->model_image = $img->image;
                } else {
                    $datas->model_image = null;
                }
            }

            return response([
                'message' => 'successfully',
                'data' => $data,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function searchhistory()
    {
        try {
            $data = SearchHistoryModel::where('user_id', Auth::id())->get();

            return response([
                'message' => 'successfully',
                'data' => $data,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function searchHistoryClear(Request $request)
    {
        try {
            SearchHistoryModel::where('user_id', Auth::id())->delete();

            return response([
                'message' => 'successfully',
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function comparecars(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id1' => 'required|numeric',
            'car_id2' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->errors()->first(),
                ], 400);
            }
            $id = [];
            array_push($id, $request->car_id1);
            array_push($id, $request->car_id2);

            $cars = Cars::comparetwocars($id);
            $id = Auth::id();
            foreach ($cars as $car) {
                $user = FavoritCars::where('user_id', $id)->where('car_id', $car->id)->first();
                if ($user) {
                    $car->isfavourit = '1';
                } else {
                    $car->isfavourit = '0';
                }
                $carimg = CarsImage::where('car_id', $car->id)->get();
                $car->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'compareCars' => $cars,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function search(Request $request)
    {
        try {
            // print_r($request->brand); die;
            $data = DB::table('cars')
                        ->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')
                        ->join('car_models', 'cars.model_id', '=', 'car_models.id')
                        ->join('car_body', 'cars.body_type_id', '=', 'car_body.id');
            if ($request->brand) {
                $data = $data->where('cars.brand_id', $request->brand);
            }
            if ($request->model) {
                $data = $data->where('cars.model_id', $request->model);
            }
            if ($request->year) {
                $data = $data->where('cars.year', $request->year);
            }
            if ($request->color) {
                $data = $data->where('cars.interior_color_id', $request->color);
            }
            $data = $data->get();

            foreach ($data as $img) {
                $carimg = CarsImage::where('car_id', $img->id)->get();
                $img->images = $carimg;
            }

            return response([
                'message' => 'successfully',
                'all_cars' => $data,
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function publishStatus(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'car_id' => 'required|numeric',
            'status' => 'required|numeric',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                'message' => $validator->errors()->first(),
                ], 400);
            }

            $getusercar = Cars::where('user_id', Auth::id())->where('id', $request->car_id)->first();
            if ($getusercar) {
                Cars::where('user_id', Auth::id())->where('id', $request->car_id)->update(['publish_status' => $request->status]);
                if ($request->status == 1) {
                    return response([
                        'status' => 200,
                        'message' => 'Car publish successfully',
                    ], 200);
                } else {
                    return response([
                        'status' => 200,
                        'message' => 'Car unPublish successfully',
                    ], 200);
                }
            } else {
                return response([
                    'status' => 200,
                    'message' => 'User not exist this car.',
                ], 200);
            }
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function favouritecriteria(Request $request)
    {
        try {
            $obj = [
                    'user_id' => Auth::id(),
                    'brand_id' => $request->brand_id,
                    'model_id' => $request->model_id,
                    'year' => $request->year,
                    'import' => $request->import,
                    'price' => $request->price,
                    'mileage' => $request->mileage,
                    'cylinders_id' => $request->cylinders_id,
                    'transmission_id' => $request->transmission_id,
                    'body_type_id' => $request->body_type_id,
                    'exterior_color_id' => $request->exterior_color_id,
                    'interior_color_id' => $request->interior_color_id,
                    'description' => $request->description,
                    'approved_by_supplier' => $request->approved_by_supplier,
                    'timely_maintenance' => $request->timely_maintenance,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
            ];

            $carid = FavouriteCriteriaModel::insertGetId($obj);

            return response([
                'message' => 'successfully',
                'addcar' => $obj,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function usernotification(Request $request){

        $alldata = Notification::where('user_id',Auth::id())->get();
        return response([
        'message' => 'successfully',
        'addcar' => $alldata,
        ], 200);

    }



    //////////////////////////////////////////// Push Notification ////////////////////////////////////////////


    public function pushNotification($userid)
    {
       
        // try {
            $usertoken = User::where('id',$userid)->first();
            $message['title'] = 'Favorite Car';
            $message['body'] = 'Favorite Car Available';
            $message['type'] = 'Car';
            $registrationIds1 = $usertoken->device_token;
            $registrationIds = [$registrationIds1];
            $fields = [
                'registration_ids' => $registrationIds,
                'data' => [
                    'title' => $message['title'],
                    'message' => $message['body'],
                    'type' => $message['type'],
                ],
                'notification' => [
                    'title' => $message['title'],
                    'body' => $message['body'],
                    'sound' => 'default',
                ],
            ];
            $headers = [
                        'Authorization: key=AAAA76aZLt0:APA91bGCJeoKRH42kuBJ1qpGhd6AFOH15WMBnJt0EukWp2YShXc1M4tPcv5wMcQmlGzwg-fbHRAt7ERcgZzMF8I0ufjzHuAdWrMaoKvJjNXSoNCBb_l0EdVvZf-tVugcMzrDKATozqFe',
                        'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result);

            Notification::insert(['title'=> $message['title'],'description'=>$message['body'],'user_id'=>$userid]);

            if ($data->success == 1) {
                return true;
            } else {
                return false;
            }
        // } catch (\Exception $e) {
        //     return response([
        // 'msg' => 'Something went wrong',
        //  ], 500);
        // }
    }

}
