<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cards;
use App\Models\SubscribePlanModel;
use App\Models\User;
use App\Models\Usersubscribes;
use Auth;
use Illuminate\Http\Request;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'phone' => 'required|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|max:20|same:password',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $User = $request->all();
            $obj = [
            'phone' => $request->phone,
            'email' => $request->email,
            'name' => $request->name,
            'password' => bcrypt($request->password),
        ];
            $data = User::insert($obj);

            $credentials = ['email' => $request->email, 'password' => $request->password];

            if (!auth()->attempt($credentials)) {
                return response(['status' => 0, 'msg' => 'Invalid Credentials'], 500);
            }
            $user = User::where('email', $request->email)->first();
            $accessToken = $user->createToken($request->email)->plainTextToken;

            return response([
                'msg' => 'You have successfully account created.',
                'token_type' => 'Bearer',
                'access_token' => $accessToken,
                'data' => Auth::user(),
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
                 ], 500);
        }
    }

    public function checkSubscription(Request $request)
    {
        $usersub = Usersubscribes::where('user_id', Auth::id())->where('is_active', 1)->first();
        if ($usersub) {
            $amount = SubscribePlanModel::where('id', $usersub->subscribes_id)->first();
            $usersub['amount'] = $amount->amount;
        }

        return response([
            'msg' => 'You Subscription Expire',
            'data' => $usersub,
        ], 200);
    }

    public function removeCard(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }
            Cards::where('id', $request->id)->where('user_id', Auth::id())->delete();

            return response([
                'msg' => 'Remove Card Successfully. ',
            ], 200);
        } catch (\Exception $e) {
            return response([
                'msg' => 'Something went wrong',
                ], 500);
        }
    }
}
