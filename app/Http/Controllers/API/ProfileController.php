<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ContactUsModel;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use SendGrid\Mail\From as From;
use SendGrid\Mail\Mail as Mail;
use SendGrid\Mail\To as To;
use Storage;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'avatar' => 'required|mimes:jpeg,jpg,png,webp|max:8048',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $data = $request->all();

            $files = $request->file('avatar');
            if ($files) {
                $filename = uniqid().'.'.$files->getClientOriginalExtension();

                $path = $files->storeAs('public/avatar', $filename);
                $data['avatar'] = asset(Storage::url($path));
            }

            $data = User::where('id', Auth::id())->update($data);
            $data = User::find(Auth::id());

            return response([
                'msg' => 'You have successfully account updated.',
                'data' => $data,
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'msg'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function change_password(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'recent_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6|max:20|same:new_password',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            $pwd = User::where('email', Auth::user()->email)->value('password');
            if (!Hash::check($request->recent_password, $pwd)) {
                return response()->json(['msg' => 'Something went wrong.'], 500);
            }

            User::where('id', Auth::id())->update(['password' => bcrypt($request->new_password)]);

            return response([
                'msg' => 'You have successfully password changed.',
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            $user = Auth::user();
            // Revoke current user token
            $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();

            return response([
                'msg' => 'You have successfully logout.',
            ], 200);
        } catch (\Exception $e) {
            return response([
                'msg' => $e->getMessage(),
                // 'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function change_email(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'recent_email' => 'required',
            'email' => 'required|unique:users',
            'confirm_email' => 'required|same:email',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            User::where('id', Auth::id())->update(['email' => $request->email]);

            return response([
                'msg' => 'You have successfully email changed.',
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function forgot_password(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            // print_r($request->email); die;

            $userdata = User::where('email', $request->email)->first();

            if ($userdata) {
                $id = base64_encode($userdata->id);
                $current_timestamp = Carbon::now()->timestamp;
                $newLink = "http://15.207.252.146/reset-password/$current_timestamp/$id";
                try {
                    $email = $userdata->email;
                    $name = 'Car Market';
                    $from = new from('algharabally360@gmail.com', 'Car Market');
                    $tos = [new To(
                    $email,
                    $name,
                    [
                    'subject' => 'Forgot Password.',
                    'link' => $newLink,
                    ])];
                    $email = new Mail($from, $tos);
                    $email->setTemplateId('d-83f3f0c58f404405ad904be4da0a597d');
                    $sendgrid = new \SendGrid('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');
                    try {
                        $response = $sendgrid->send($email);
                        $returndata = 'success';
                    } catch (Exception $e) {
                        $returndata = 'failes';
                    }
                } catch (Exception $ex) {
                }
                if ($returndata == 'success') {
                    return response()->json(['status' => 'success', 'status_code' => 200,
                'msg' => 'Reset password link sent to your email please check email and reset your password.',
                ]);
                } else {
                    return response()->json(['status' => 'success', 'status_code' => 400, 'msg' => 'Something is went wrong']);
                }
            } else {
                return response()->json(['status_code' => 400, 'msg' => 'User not exist'], 400);
            }
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function userprofile(Request $request)
    {
        $user = User::where('id', Auth::id())->first();

        if ($user) {
            return response([
                'msg' => ' successfully .',
                'data' => $user,
            ], 200);
        } else {
            $user = null;

            return response([
                'msg' => ' user not exist .',
                'data' => $user,
            ], 400);
        }
    }

    public function updateprofile(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'dob' => 'required|date',
            'gender' => 'required',
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }

            User::where('id', Auth::id())->update(['name' => $request->name, 'dob' => $request->dob, 'gender' => $request->gender]);

            return response([
                'msg' => 'You have successfully update .',
            ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'msg' => 'Something went wrong',
            ], 500);
        }
    }

    public function reportIssue(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'issue' => 'required',
            'car_id'=> 'required', 
        ]);

        try {
            if ($validator->fails()) {
                return response()->json([
                    'msg' => $validator->errors()->first(),
                ], 400);
            }
            ContactUsModel::insert(['user_id' => Auth::id(), 'complaint' => $request->issue,'car_id'=> $request->car_id]);

            return response([

                'msg' => 'You Issue Send to Admin Successfully .',
            ], 200);
        } catch (\Exception $e) {
            return response([
                'msg' => 'Something went wrong',
            ], 500);
        }
    }
}
