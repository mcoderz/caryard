<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cards;
use App\Models\SubscribePlanModel;
use App\Models\User;
use App\Models\Usersubscribes;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ApiTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $request->validate([
            'email' => 'required',
            'password' => 'required',
            'device_name' => 'required',
        ]);
            if ($request->email) {
                $user = User::where('email', $request->email)->first();
                if (empty($user)) {
                    return response([
                'msg' => 'User not exist.',
                          ], 500);
                }
            } else {
                $user = User::where('email', $request->email)->first();
            }

            if (!$user || !Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
            }

            $token = $user->createToken($request->device_name)->plainTextToken;

            return response([
                   'token_type' => 'Bearer',
                   'access_token' => $token,
                   'user' => $user,
                   ], 200)
                  ->header('Content-Type', 'text/plain')
                  ->header('X-Custom-header', 'hey');
        } catch (\Exception $e) {
            return response([
                    'msg' => $e->getMessage(),
                // 'msg'=>'Something went wrong',
                          ], 500);
        }
    }

    public function updatetoken(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'token' => 'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            User::where('id', Auth::id())->update(['device_token' => $request->token]);

            return response([
                'message' => 'Token Update successfully',
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function pushNotification()
    {
        try {
            $message['title'] = 'TeST noti';
            $message['body'] = 'hello';
            $message['type'] = 'Car';
            $registrationIds1 = 'eOffBWTBSDWbY4Dd54NLhC:APA91bFUkQvlkrp65zCvWxOnK4FmCMoo5zL5BHwFnSPu1ZlCbLoHDpex0Q_MwrQbCo8GfIAXCqxWLiom2y4htLpqe__V8CRlAePNJ6HszQC-dp4okEPgl4I1I1kgkpZ8EnEhetiYsx39';
            $registrationIds = [$registrationIds1];
            $fields = [
                'registration_ids' => $registrationIds,
                'data' => [
                    'title' => $message['title'],
                    'message' => $message['body'],
                    'type' => $message['type'],
                ],
                'notification' => [
                    'title' => $message['title'],
                    'body' => $message['body'],
                    'sound' => 'default',
                ],
            ];
            $headers = [
                        'Authorization: key=AAAA76aZLt0:APA91bGCJeoKRH42kuBJ1qpGhd6AFOH15WMBnJt0EukWp2YShXc1M4tPcv5wMcQmlGzwg-fbHRAt7ERcgZzMF8I0ufjzHuAdWrMaoKvJjNXSoNCBb_l0EdVvZf-tVugcMzrDKATozqFe',
                        'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result);

            // if ($data->success == 1) {
            //     return true;
            // } else {
            //     return false;
            // }
        } catch (\Exception $e) {
            return response([
        'msg' => 'Something went wrong',
         ], 500);
        }
    }

    public function allSubscribePlans()
    {
        try {
            $allplan = SubscribePlanModel::where('status', 1)->get();

            return response([
                'message' => 'Successfully',
                'data' => $allplan,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function paynow(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            // 'name' => 'required',
            // 'card_number' => 'required',
            // 'expiry_month' => 'required',
            // 'expiry_year' => 'required',
            'sub_id'=>   'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }
            // $obj = [
            //         'user_id' => Auth::id(),
            //         'card_holder_name' => $request->name,
            //         'card_number' => $request->card_number,
            //         'expiry_month' => $request->expiry_month,
            //         'expiry_year' => $request->expiry_year,
            //         'cvv' => $request->cvv,
            //     ];
            // if ($request->save_card == 1) {
            //     $allplan = Cards::insert($obj);
            // }
            $checkactivesub = Usersubscribes::where('user_id', AUth::id())->where('is_active', 1)->first();

            if (empty($checkactivesub)) {
                $plandata = SubscribePlanModel::where('id', $request->sub_id)->first();
                $enddate = date('Y-m-d', strtotime("$plandata->month months", strtotime(date('Y-m-d'))));
                $usersubid = Usersubscribes::insertGetId(['user_id' => Auth::id(), 'subscribes_id' => $request->sub_id, 'start_date' => date('Y-m-d'), 'end_date' => $enddate, 'post_count' => $plandata->post_count, 'is_active' => 0]);
                $responsepayment = $this->payment( $plandata->amount);
                $data = new \StdClass();
                $data->url = $responsepayment['paymentURL'];
                $data->id = $usersubid;

                return response([
                    'message' => 'Pay Successfully',
                    'data' => $data,
                    ], 200);
            }
            return response([
                'message' => 'All Ready Paid.',
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function userAllcards(Request $request)
    {
        try {
            $allplan = Cards::where('user_id', AUth::id())->get();

            return response([
                'message' => 'Successfully',
                'data' => $allplan,
                ], 200);
        } catch (\Exception $e) {
            return response([
                // 'error'=>$e->getMessage(),
                'message' => 'Something went wrong',
            ], 500);
        }
    }

    public function payment($amount){

        $fields = array(
            'merchant_id'=>'1201',
             'username' => 'test',
            'password'=>stripslashes('test'), 
            'api_key'=>'jtest123', // in sandbox request
             
            'order_id'=>time(), 
            'total_price'=>$amount,
            'CurrencyCode'=>'KWD',
            'CstFName'=>'Test Name',
            'CstEmail'=>'test@test.com',
            'CstMobile'=>'12345678',
            'success_url'=>'https://example.com/success.html', 
            'error_url'=>'https://example.com/error.html', 
            'test_mode'=>1, // test mode enabled
            'whitelabled'=>true, // only accept in live credentials (it will not work in test)
            'payment_gateway'=>'knet',// only works in production mode
            'ProductName'=>json_encode(['computer','television']),
            'ProductQty'=>json_encode([2,1]),
            'ProductPrice'=>json_encode([150,1500]),
            'reference'=>'Ref00001'
            );


            $fields_string = http_build_query($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://api.upayments.com/test-payment");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);
            $server_output = json_decode($server_output,true);
           return  $server_output;
            
    }

    public function activeplan(Request $request){
        $validator = \Validator::make($request->all(), [
            
            'id'=>   'required',
            'is_active'=>   'required',

        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                ], 400);
            }

            Usersubscribes::where('id',$request->id)->update(['is_active'=>$request->is_active]);
            return response([
                'message' => 'Successfully.',
                ], 200);
        } catch (\Exception $e) {
            return response([
                'message' => 'Something went wrong',
            ], 500);
        }  

    }
}
