<?php

namespace App\Http\Controllers;

use App\Models\CarsImage;
use App\Models\Userlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Userlistcontroller extends Controller
{
    public function userManagement()
    {
        $allUsers = Userlist::orderBy('id', 'DESC')->get();

        return view('userlist', compact('allUsers'));
    }

    public function index()
    {
        return view('usermanagement');
    }

    public function viewUserdata($id)
    {
        // $allData = Userlist::where('id', $id)->first();
        $allcars = DB::table('cars')->join('car_brand', 'cars.brand_id', '=', 'car_brand.id')->join('car_models', 'cars.model_id', '=', 'car_models.id')->join('cylinders', 'cars.cylinders_id', '=', 'cylinders.id')->join('transmission', 'cars.transmission_id', '=', 'transmission.id')->join('car_body', 'cars.body_type_id', '=', 'car_body.id')->join('interior_color', 'cars.interior_color_id', '=', 'interior_color.id')->join('exterior_color', 'cars.exterior_color_id', '=', 'exterior_color.id')->select('cars.id', 'cars.year', 'cars.import', 'cars.price', 'cars.mileage', 'cars.description', 'car_brand.brand_name', 'car_models.model_name', 'cylinders.cylinder', 'transmission.transmission_name', 'car_body.body_name', 'interior_color.int_color', 'exterior_color.ext_color')->where('cars.user_id',$id)->get();
        foreach($allcars as $allcar){
           $carimag =  CarsImage::where('car_id',$allcar->id)->get();
            $allcar->images = $carimag;
        }
        // print_r( $allcars); die;

        return view('user_view', compact('allcars'));
    }

    public function userStatus(Request $requeat)
    {
        $id = $requeat->id;
        $status = $requeat->status;
        Userlist::where('id', $id)->update(['status' => $status]);
    }


    public function payment(){

        $fields = array(
            'merchant_id'=>'1201',
             'username' => 'test',
            'password'=>stripslashes('test'), 
            'api_key'=>'jtest123', // in sandbox request
             
            'order_id'=>time(), 
            'total_price'=>'10',
            'CurrencyCode'=>'KWD',
            'CstFName'=>'Test Name',
            'CstEmail'=>'test@test.com',
            'CstMobile'=>'12345678',
            'success_url'=>'https://example.com/success.html', 
            'error_url'=>'https://example.com/error.html', 
            'test_mode'=>1, // test mode enabled
            'whitelabled'=>true, // only accept in live credentials (it will not work in test)
            'payment_gateway'=>'knet',// only works in production mode
            'ProductName'=>json_encode(['computer','television']),
            'ProductQty'=>json_encode([2,1]),
            'ProductPrice'=>json_encode([150,1500]),
            'reference'=>'Ref00001'
            );


            $fields_string = http_build_query($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://api.upayments.com/test-payment");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close($ch);
            // print_r($server_output); 
            $server_output = json_decode($server_output,true);
            print_r($server_output['paymentURL'] ); die;
            
    }

   
}
