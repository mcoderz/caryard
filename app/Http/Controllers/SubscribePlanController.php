<?php

namespace App\Http\Controllers;

use App\Models\SubscribePlanModel;
use Illuminate\Http\Request;

class SubscribePlanController extends Controller
{
    public function subscribePlan()
    {
        $allSubscribe = SubscribePlanModel::where('softdelete',1)->get();

        return view('subscribePlan', compact('allSubscribe'));
    }

    public function addSubscribePlan(Request $request)
    {
        SubscribePlanModel::insert(['plan_name' => $request->name, 'amount' => $request->amount, 'month' => $request->month, 'post_count' => $request->postcount]);

        return back();
    }

    public function editSubscribePlan(Request $request)
    {
        SubscribePlanModel::where('id', $request->id)->update(['plan_name' => $request->name, 'amount' => $request->amount, 'month' => $request->month, 'post_count' => $request->postcount]);

        return back();
    }

    public function SubscribePlanDelete(Request $request)
    {
        SubscribePlanModel::where('id', $request->id)->update(['softdelete'=>0]);
    }
}
