<?php

namespace App\Http\Controllers;

use App\Models\Cars;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function checklogin(Request $request)
    {
        $this->validate($request, [
      'email' => 'required|email',
      'password' => 'required',
     ]);

        //  $user_data = [
        //   'email' => $request->get('email'),
        //   'password'=> $request->get('password')
        //  ];
        //  $credentials = $request->only('email', 'password');
        //  $data=Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
        $data = Auth::guard('adminss')->attempt(['email' => $request->email, 'password' => $request->password]);
        if (!empty($data)) {
            $daycount = Cars::whereDate('created_at',date('Y-m-d'))->count();
            $weekcount = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 week")))->count();
            $monthcount = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 month")))->count();
            $totalcard = Cars::count();
            // sold
            $daysold = Cars::whereDate('created_at',date('Y-m-d'))->where('status',2)->count();
            $weeksold = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 week")))->where('status',2)->count();
            $monthdold = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 month")))->where('status',2)->count();
            $totalsold = Cars::where('status',2)->count();

            $allusers = User::count();

            $data = [
                'daycount'=>$daycount,
                'monthcount'=>$monthcount,
                'weekcount'=>$weekcount,
                'totalcard'=>$totalcard,
                'daysold'=>$daysold,
                'weeksold'=>$weeksold,
                'monthdold'=>$monthdold,
                'totalsold'=>$totalsold,
                'allusers'=>$allusers
            ];
            // print_r($data); die;
            return view('carlist',compact('data'));
        } else {
            return back()->with('error', 'Wrong Login Details');
        }
    }

    // function successlogin()
    // {
    //     return view('successlogin');
    // }
    public function carlist()
    { 
        $daycount = Cars::whereDate('created_at',date('Y-m-d'))->count();
        $weekcount = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 week")))->count();
        $monthcount = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 month")))->count();
        $totalcard = Cars::count();
        // sold
        $daysold = Cars::whereDate('created_at',date('Y-m-d'))->where('status',2)->count();
        $weeksold = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 week")))->where('status',2)->count();
        $monthdold = Cars::whereDate('created_at','<=',date('Y-m-d'))->whereDate('created_at','>=',date("Y-m-d", strtotime("-1 month")))->where('status',2)->count();
        $totalsold = Cars::where('status',2)->count();

        $allusers = User::count();

        $data = [
            'daycount'=>$daycount,
            'monthcount'=>$monthcount,
            'weekcount'=>$weekcount,
            'totalcard'=>$totalcard,
            'daysold'=>$daysold,
            'weeksold'=>$weeksold,
            'monthdold'=>$monthdold,
            'totalsold'=>$totalsold,
            'allusers'=>$allusers
        ];
        return view('carlist',compact('data'));
    }

    public function planlist()
    {
        return view('planlist');
    }

    public function newcaradd()
    {
        return view('newcar');
    }

    public function contacthistory()
    {
        return view('contacthistory');
    }

    public function forgotpassword()
    {
        return view('forgotpassword');
    }

    public function signup()
    {
        return view('signup');
    }
}
