<?php

use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\FavoriteCriteriaController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\SubscribePlanController;
use App\Http\Controllers\Userlistcontroller;
use App\Http\Controllers\userviewcontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/logout', function () {
    Auth::guard('adminss')->logout();

    return redirect('/admin');
});
Route::get('logout', function () {
    Auth::guard('adminss')->logout();

    return redirect('/admin');
});
Route::get('reset-password/{timeStamp}/{id}', [ForgotPasswordController::class, 'showResetForm']);
Route::post('resetPassword', [ForgotPasswordController::class, 'resetPassword']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// route to process the form
Route::get('/admin', [MainController::class, 'index']);
Route::post('/admin/dashboard', [MainController::class, 'checklogin']);
//Route::get('/admin/dashboard', [MainController::class, 'successlogin']);
Route::get('/admin/userlist', [Userlistcontroller::class, 'userManagement'])->middleware('AdminLogin');
// contact us
Route::get('/admin/contactus', [ContactUsController::class, 'contactus'])->middleware('AdminLogin');
Route::post('/admin/replyToUser', [ContactUsController::class, 'replyToUser']);
// subscribePlan
Route::get('admin/subscribePlan', [SubscribePlanController::class, 'subscribePlan'])->middleware('AdminLogin');
Route::post('/admin/addSubscribePlan', [SubscribePlanController::class, 'addSubscribePlan']);
Route::post('/admin/editSubscribePlan', [SubscribePlanController::class, 'editSubscribePlan']);
Route::post('/admin/SubscribePlanDelete', [SubscribePlanController::class, 'SubscribePlanDelete']);
// invoice
Route::get('admin/invoice', [InvoiceController::class, 'invoice'])->middleware('AdminLogin');
// criteria
Route::get('/admin/criteria', [FavoriteCriteriaController::class, 'criteria'])->middleware('AdminLogin');
Route::get('/favorite_criteria/{any}', [FavoriteCriteriaController::class, 'viewfavoritecriteria'])->middleware('AdminLogin');


// Route::get('/admin/usermanagement', [Userlistcontroller::class, 'index']);
// Route::get('/admin/userview', [userviewcontroller::class, 'index']);
// Route::get('/admin/userviewlist', [userviewcontroller::class, 'userview']);
Route::get('/admin/dashboard', [MainController::class, 'carlist'])->middleware('AdminLogin');
Route::get('/user_view/{any}', [Userlistcontroller::class, 'viewUserdata'])->middleware('AdminLogin');
Route::post('userStatus', [Userlistcontroller::class, 'userStatus']);
Route::get('/admin/planlist', [MainController::class, 'planlist'])->middleware('AdminLogin');
Route::get('/admin/newcar', [MainController::class, 'newcaradd'])->middleware('AdminLogin');
Route::get('/admin/contacthistory', [MainController::class, 'contacthistory'])->middleware('AdminLogin');

Route::get('/admin/blankpage', [MainController::class, 'blankpage'])->middleware('AdminLogin');
Route::get('/admin/forgotpassword', [MainController::class, 'forgotpassword']);
Route::get('/signup', [MainController::class, 'signup']);

Route::get('/admin/carmodel', function(){
    return view('carmodel');
});